﻿using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using NguyenHuuKhanhHoang_DE_API;
using NguyenHuuKhanhHoang_DE_API.Model.AuthModel;
using NguyenHuuKhanhHoang_DE_API.Model.CommentViewModel;
using NguyenHuuKhanhHoang_DE_API.Model.LoginModel;
using NguyenHuuKhanhHoang_DE_API.Model.NewsViewModel;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang.UnitTest
{
    public class NashTechTest
    {
        protected readonly HttpClient Client;
        public NashTechTest()
        {
            var appFactory = new WebApplicationFactory<Startup>();
            Client = appFactory.CreateClient();
        }
        protected async Task AuthenticationAsync(string userName)
        {
            Client.DefaultRequestHeaders.Authorization =new AuthenticationHeaderValue("bearer", await GetJWTToken(userName));
        }
        private async Task<string> GetJWTToken(string userName)
        {
            var user = new LoginModel()
            {
                UserName = userName,
                Password ="123"
            };
            var data = JsonConvert.SerializeObject(user);
            var content = new StringContent(data);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Login/Login", content);
            var responseData = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<AccessUser>(responseData);
            return result.AccessToken.Token;
        }
        public async Task<HttpResponseMessage> CreateBaiVietAsync(UpLoadNewsViewModel upload)
        {
            var data = JsonConvert.SerializeObject(upload);
            var content = new StringContent(data);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Home/DangBaiViet", content);

            return response;

        }
        public async Task<HttpResponseMessage> UpdateBaiVietAsync(UpDateNewsViewModel updateNews)
        {
            var data = JsonConvert.SerializeObject(updateNews);
            var content = new StringContent(data);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Home/UpdateBaiViet", content);

            return response;

        }
        public async Task<HttpResponseMessage> CommentBaiVietAsync(CommentViewModel commentNews)
        {
            var data = JsonConvert.SerializeObject(commentNews);
            var content = new StringContent(data);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Home/Comment", content);

            return response;

        }
        #region LoginControllerTest
        public async Task<HttpResponseMessage> LoginAsync(LoginModel loginViewModel)
        {
            var data = JsonConvert.SerializeObject(loginViewModel);
            var content = new StringContent(data);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Login/Login", content);

            return response;

        }
        public async Task<HttpResponseMessage> RegisterAsync(RegisterViewModel registerViewModel)
        {
            var data = JsonConvert.SerializeObject(registerViewModel);
            var content = new StringContent(data);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Login/Register", content);

            return response;

        }
        #endregion
    }
}
