﻿using FluentAssertions;
using NguyenHuuKhanhHoang_DE_API.Model.LoginModel;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NguyenHuuKhanhHoang.UnitTest
{
    public class LoginControllerTest : NashTechTest
    {
        [Fact]
        public async Task LoginTest()
        {
            var user = new LoginModel()
            {
               UserName = "Admin",
               Password = "123"
            };
            var response = await LoginAsync(user);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task LoginTestCaseWrongUserOrPass()
        {
            var user = new LoginModel()
            {
                UserName = "WrongUser",
                Password = "123"
            };
            var response = await LoginAsync(user);
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
        [Fact]
        public async Task LoginTestCaseMissUserOrPass()
        {
            var user = new LoginModel()
            {
                UserName = "WrongUser",
               // Password = "123"
            };
            var response = await LoginAsync(user);
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
        [Fact]
        public async Task RegisterTest()
        {
            var user = new RegisterViewModel()
            {
                UserName = "Test Unit Test",
                Password = "123",
                ConfirmPassword ="123",
                GioiThieu = "Test Unit Test"
            };
            var response = await RegisterAsync(user);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task RegisterTestCaseMissField()
        {
            var user = new RegisterViewModel()
            {
                UserName = "Test Unit Test",
                Password = "123",
                ConfirmPassword = "123",
                //GioiThieu = "Test Unit Test"
            };
            var response = await RegisterAsync(user);
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
        [Fact]
        public async Task RegisterTestCaseExistUser()
        {
            var user = new RegisterViewModel()
            {
                UserName = "User",
                Password = "123",
                ConfirmPassword = "123",
                GioiThieu = "Test Unit Test"
            };
            var response = await RegisterAsync(user);
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
        [Fact]
        public async Task RegisterTestCaseNotMatchPassAndConfirmPass()
        {
            var user = new RegisterViewModel()
            {
                UserName = "User",
                Password = "123",
                ConfirmPassword = "1234",
                GioiThieu = "Test Unit Test"
            };
            var response = await RegisterAsync(user);
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
    }
}
