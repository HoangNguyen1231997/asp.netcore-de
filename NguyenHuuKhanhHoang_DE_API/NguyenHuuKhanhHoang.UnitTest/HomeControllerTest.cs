﻿using FluentAssertions;
using Newtonsoft.Json;
using NguyenHuuKhanhHoang_DE_API.Model.AuthModel;
using NguyenHuuKhanhHoang_DE_API.Model.CommentViewModel;
using NguyenHuuKhanhHoang_DE_API.Model.NewsViewModel;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NguyenHuuKhanhHoang.UnitTest
{
    public class HomeControllerTest:NashTechTest
    {
        [Fact]
        public async Task GetHeaderNews()
        {
            await AuthenticationAsync("Admin");
            var response = await Client.GetAsync("/Home/GetHeaderNews");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task GetBodyNews()
        {
            await AuthenticationAsync("Admin");
            var response = await Client.GetAsync("/Home/GetBodyNews");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task GetDetailNews()
        {
            await AuthenticationAsync("Admin");
            var response = await Client.GetAsync("/Home/GetDetailNews?id=1");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task GetDetailNewsNotFound()
        {
            await AuthenticationAsync("Admin");
            var response = await Client.GetAsync("/Home/GetDetailNews?id=100");
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
        [Fact]
        public async Task GetDetailNewsCaseUnauthorized()
        {
            var response = await Client.GetAsync("/Home/GetDetailNews?id=1");
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
        [Fact]
        public async Task GetDetailUser()
        {
            await AuthenticationAsync("Admin");
            var response = await Client.GetAsync("/Home/GetDetailUser?id=1");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task GetDetailUserCaseUnauthorized()
        {
            var response = await Client.GetAsync("/Home/GetDetailUser?id=1");
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
        [Fact]
        public async Task DangBaiVietCaseUnauthorized()
        {
            var newsEntity = new UpLoadNewsViewModel()
            {
                TieuDe = "Test Unit Test",
                TheLoai = "Test Unit Test",
                Type = 2,
                UserId = 1,
                ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
               ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
               NoiDung = "Test Unit Test",
               NoiDungSub = "Test Unit Test",
                
            };
            var response = await CreateBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
        [Fact]
        public async Task DangBaiViet()
        {
            await AuthenticationAsync("Admin");
            var newsEntity = new UpLoadNewsViewModel()
            {
                TieuDe = "Test Unit Test",
                TheLoai = "Test Unit Test",
                Type = 2,
                UserId = 1,
                ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
                ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                NoiDung = "Test Unit Test",
                NoiDungSub = "Test Unit Test",

            };
            var response = await CreateBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task DangBaiVietCaseMissField()
        {
            await AuthenticationAsync("Admin");
            var newsEntity = new UpLoadNewsViewModel()
            {
                TieuDe = "Test Unit Test",
               // TheLoai = "Test Unit Test",
                Type = 2,
                UserId = 1,
                ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
                ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                NoiDung = "Test Unit Test",
                NoiDungSub = "Test Unit Test",

            };
            var response = await CreateBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
        [Fact]
        public async Task DangBaiVietCaseForbidden()
        {
            await AuthenticationAsync("User");
            var newsEntity = new UpLoadNewsViewModel()
            {
                TieuDe = "Test Unit Test",
                TheLoai = "Test Unit Test",
                Type = 2,
                UserId = 1,
                ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
                ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                NoiDung = "Test Unit Test",
                NoiDungSub = "Test Unit Test",

            };
            var response = await CreateBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
        }
        [Fact]
        public async Task UpdateBaiVietCaseUnauthorized()
        {
            var newsEntity = new UpDateNewsViewModel()
            {
                Id = 5,
                TieuDe = "Test Update Unit Test",
                TheLoai = "Test Update Unit Test",
                Type = 2,
                UserId = 1,
                NewDetailId = 4,
                ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
                ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                NoiDung = "Test Update Unit Test",
                NoiDungSub = "Test Update Unit Test",

            };
            var response = await UpdateBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
        [Fact]
        public async Task UpdateBaiViet()
        {
            await AuthenticationAsync("Admin");
            var newsEntity = new UpDateNewsViewModel()
            {
                Id = 5,
                TieuDe = "Test Update Unit Test",
                TheLoai = "Test Update Unit Test",
                Type = 2,
                UserId = 1,
                NewDetailId = 4,
                ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
                ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                NoiDung = "Test Update Unit Test",
                NoiDungSub = "Test Update Unit Test",

            };
            var response = await UpdateBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task UpdateBaiVietCaseMissField()
        {
            await AuthenticationAsync("Admin");
            var newsEntity = new UpDateNewsViewModel()
            {
                Id = 5,
                TieuDe = "Test Update Unit Test",
                //TheLoai = "Test Update Unit Test",
                Type = 2,
                UserId = 1,
                NewDetailId = 4,
                ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
                ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                NoiDung = "Test Update Unit Test",
                NoiDungSub = "Test Update Unit Test",

            };
            var response = await UpdateBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
        [Fact]
        public async Task UpdateBaiVietCaseForbidden()
        {
            await AuthenticationAsync("User");
            var newsEntity = new UpDateNewsViewModel()
            {
                Id = 5,
                TieuDe = "Test Update Unit Test",
                TheLoai = "Test Update Unit Test",
                Type = 2,
                UserId = 1,
                NewDetailId = 4,
                ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                ImageUrlSub = "http://nashtechglobal.com/wp-content/uploads/2018/11/DSC_6092.jpg",
                ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                NoiDung = "Test Update Unit Test",
                NoiDungSub = "Test Update Unit Test",

            };
            var response = await UpdateBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
        }
        [Fact]
        public async Task DeleteNew()
        {
            await AuthenticationAsync("Admin");
            var content = new StringContent("Test");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Home/DeleteNew?id=6",content);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task DeleteNewCaseForbidden()
        {
            await AuthenticationAsync("User");
            var content = new StringContent("Test");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Home/DeleteNew?id=6", content);
            response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
        }
        [Fact]
        public async Task DeleteNewCaseUnauthorized()
        {
            var content = new StringContent("Test");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.PostAsync("/Home/DeleteNew?id=6", content);
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
        [Fact]
        public async Task GetDetailNewForUpdate()
        {
            await AuthenticationAsync("Admin");
            var response = await Client.GetAsync("/Home/GetDetailNewForUpdate?id=1");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task GetDetailNewForUpdateNotFound()
        {
            await AuthenticationAsync("Admin");
            var response = await Client.GetAsync("/Home/GetDetailNewForUpdate?id=100");
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
        [Fact]
        public async Task GetDetailNewForUpdateCaseForbidden()
        {
            await AuthenticationAsync("User");
            var response = await Client.GetAsync("/Home/GetDetailNewForUpdate?id=1");
            response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
        }
        [Fact]
        public async Task GetDetailNewForUpdateCaseUnauthorized()
        {
            var response = await Client.GetAsync("/Home/GetDetailNewForUpdate?id=1");
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
        [Fact]
        public async Task CommentBaiViet()
        {
            await AuthenticationAsync("Admin");
            var newsEntity = new CommentViewModel()
            {
                NewsId = 1,
                UserId = 1,
                NoiDung = "Test Unit Test",
                UserName = "Admin"

            };
            var response = await CommentBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
       
        [Fact]
        public async Task CommentBaiVietCaseUnauthorized()
        {
            var newsEntity = new CommentViewModel()
            {
                NewsId = 1,
                UserId = 1,
                NoiDung = "Test Unit Test",
                UserName = "Admin"

            };
            var response = await CommentBaiVietAsync(newsEntity);
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
    }
}
