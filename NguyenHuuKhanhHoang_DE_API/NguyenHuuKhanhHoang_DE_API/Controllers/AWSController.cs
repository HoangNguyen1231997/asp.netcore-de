﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DE_API.Service.Service.AWS;

namespace NguyenHuuKhanhHoang_DE_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AWSController : Controller
    {
        private IAWSService _iAWSService;
        public AWSController(IAWSService iAWSService)
        {
            _iAWSService = iAWSService;
        }
        [HttpPost("CreateBucket")]
        public async Task<ActionResult> CreateBucket(string name)
        {
            var result = await _iAWSService.CreateBucketAmz(name);
            return Ok(result);
        }
        [HttpPost("CreateFolderBucketAsync")]
        public async Task<ActionResult> CreateFolderBucketAsync(string name)
        {
            var result = await _iAWSService.CreateFolderBucketAsync(name);
            return Ok(result);
        }
        [HttpPost("UploadFile")]
        public async Task<ActionResult> UploadFile(string name)
        {
            await _iAWSService.UploadFileAsync(name);
            return Ok();
        }
        [HttpPost("TestAWS")]
        public async Task<ActionResult> TestAWS(string name)
        {
             _iAWSService.GetPreSignedUploadFileUrl(name);
            return Ok();
        }
        [HttpPost("TestAWSIformfile")]
        public async Task<ActionResult> TestAWSIformfile(string bucketName , string folder, long userId)
        {
            var httpRequset = HttpContext.Request.Form.Files["Image"];
            await _iAWSService.TestUploadFileByIFormFile(httpRequset, bucketName, folder, userId);
            return Ok();
        }
        [HttpPost("GetObjectFromS3")]
        public async Task<ActionResult> GetObjectFromS3(string name,string keyName)
        {
            await _iAWSService.GetObjectFromS3(name, keyName);
            return Ok();
        }
        [HttpPost("GetTaiLieuUrl")]
        public  ActionResult GetTaiLieuUrl(string bucketName, string keyName)
        {
            var result =  _iAWSService.GetTaiLieuUrl(bucketName, keyName);
            return Ok(new { Value = result });
        }
        [HttpPost("DeleteBucketAsync")]
        public async Task<ActionResult> DeleteBucketAsync(string name)
        {
            await _iAWSService.DeleteBucketAsync(name);
            return Ok();
        }
        [HttpPost("DeleteObjectFromS3Async")]
        public async Task<ActionResult> DeleteObjectFromS3Async(string bucketName , string keyName)
        {
            await _iAWSService.DeleteObjectFromS3Async(bucketName, keyName);
            return Ok();
        }
    }
}