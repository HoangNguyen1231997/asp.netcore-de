﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using FirebaseAdmin.Messaging;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.Comments;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.News;
using NguyenHuuKhanhHoang_DE_API.Model.CommentViewModel;
using NguyenHuuKhanhHoang_DE_API.Model.ErrorModel;
using NguyenHuuKhanhHoang_DE_API.Model.NewsViewModel;
using NguyenHuuKhanhHoang_DE_API.Model.Notification;
using NguyenHuuKhanhHoang_DE_API.Service.Service.News;
using NguyenHuuKhanhHoang_DE_API.Service.Service.Notification;
using NguyenHuuKhanhHoang_DE_API.Service.Service.Users;

namespace NguyenHuuKhanhHoang_DE_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : Controller
    {
        private IMapper _mapper;
        private INewsService _newsService;
        private IUserService _userService;
        private INotificationService _notificationService;
        public HomeController(INewsService newsService, IUserService userService, IMapper mapper, INotificationService notificationService)
        {
            _mapper = mapper;
            _newsService = newsService;
            _userService = userService;
            _notificationService = notificationService;
        }
       
        [HttpGet("GetHeaderNews")]
        public async Task<ActionResult> GetHeaderNews()
        {
            var result = await _newsService.GetNewList(1);
            return Ok(result);
        }
        [HttpGet("GetBodyNews")]
        public async Task<ActionResult> GetBodyNews()
        {
            var result = await _newsService.GetNewList(2);
            return Ok(result);
        }
        [HttpGet("CheckToken")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> CheckToken()
        {
           
            return Ok();
        }
        [HttpGet("TestFireBase")]
        public async Task<ActionResult> TestFireBase(string token)
        {
            var url = Path.Combine(Directory.GetCurrentDirectory(), "Resources\\point-blank.mp3");
            // var check  = SubscribeToTopicAsync("Test", "d4Fzozz5pPZu0mEPYiF2n9:APA91bHAbeVt-vN8BN_37YoW7nMkHXcyf-WPh9cKS9ix8DA1GFS4lGOhvJDyihNkie6gZx7euaC3OLMj0egMTkYcMKKxiRXWACADbRZpZp1Tqh6z3y-KRlbI4fwxroNUcMB4HvVxBvrc");
            var messageAction = new FirebaseAdmin.Messaging.Action
            {
                Icon = "https://localhost:5001/Resources/ring.png",
            };
            var messageAction1 = new FirebaseAdmin.Messaging.Aps
            {
                Sound = "https://localhost:5001/Resources/goes-without-saying.m4r"
            };
            var messageAction12 = new FirebaseAdmin.Messaging.CriticalSound
            {
               Name = url,
               Volume=1.0
            };
            var message = new Message
            {
                Apns = new ApnsConfig
                {
                  Aps = new Aps
                  {
                      // CriticalSound= messageAction12,

                       Sound = "https://localhost:5001/Resources/point-blank"
                      // Sound = url
                      //Sound = "default"
                  } 
                },
                Notification = new Notification
                {
                    Body = "Test dưới BE",
                    Title = "FireBase Test"
                },
                Data = new Dictionary<string, string>()
                {
                    { "user_id", "1" },
                    { "icon", "https://localhost:5001/Resources/ring.png" }
                },
                Webpush = new WebpushConfig
                {
                    Notification = new WebpushNotification
                    {
                        Icon= "https://localhost:5001/Resources/ring.png"
                    }
                },
                Token = "d4Fzozz5pPZu0mEPYiF2n9:APA91bFLon-MgX1TMb9ckKJwOpG49ws1T6LM_w5vlL1HfvdJaxdtppaAq9krM4l29eJOgrpOz5XrrWwUPR0NqFF0715ivN_dE2zVbmbnyC761eyAxMZ8GDDdPAZCuHjOVnlgLyWKLQPh"
                // Topic ="Test"
            };
            string response = await FirebaseMessaging.DefaultInstance.SendAsync(message);
            
            return Ok();
        }
        private async Task<bool> SubscribeToTopicAsync(string topic, params string[] registrationTokens)
        {
            try
            {
                var response = await FirebaseMessaging.DefaultInstance.SubscribeToTopicAsync(registrationTokens, topic);
                return response.SuccessCount == registrationTokens.Length;
            }
            catch
            {
                return false;
            }
        }
        [HttpGet("CheckPermission")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme,Roles ="Admin")]
        public async Task<ActionResult> CheckPermission()
        {

            return Ok();
        }
        [HttpGet("GetDetailNews")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> GetDetailNews(long id)
        {
            var result = await _newsService.GetNewsDetailValue(id);
            if (result == null)
            {
                throw new ApiException("Vui lòng chọn đúng bài viết");
            }
            return Ok(result);
        }
        [HttpGet("GetDetailNewsTestAutoMapper")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> TestAutoMapper(long id)
        {
            var result = await _newsService.GetNewsDetailValue(id);
            if (result == null)
            {
                throw new ApiException("Vui lòng chọn đúng bài viết");
            }
            var resultData = _mapper.Map<Model.TestAutoMapper.News.NewsViewModel>(result);
            return Ok(resultData);
        }
        [HttpGet("GetDetailUser")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> GetDetailUser(long id)
        {
            var result = await _userService.GetDeatilUser(id);
            return Ok(result);
        }
        [HttpPost("UploadImage")]
        public ActionResult UploadImage()
        {
            string imageName = null;
            var httpRequset = HttpContext.Request.Form.Files["Image"];
            imageName = Guid.NewGuid().ToString() + "_" + httpRequset.FileName;
            string uploadFolder = "Resources/" + imageName;
            httpRequset.CopyToAsync(new FileStream(uploadFolder, FileMode.Create));
            var result = "https://localhost:5001/" + uploadFolder;
            return Ok(new { Value = result });
        }
        [HttpPost("DangBaiViet")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme,Roles ="Admin")]
        public ActionResult DangBaiViet(UpLoadNewsViewModel uploadViewModel)
        {
            var newsEntity = new News()
            {
                TieuDe = uploadViewModel.TieuDe,
                TheLoai = uploadViewModel.TheLoai,
                Type = 2,
                UserId = Convert.ToInt64(uploadViewModel.UserId),
                NewsDetail = new NewsDetail()
                {
                    ImageUrl = uploadViewModel.ImageUrl,
                    ImageUrlSub = uploadViewModel.ImageUrlSub,
                    ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                    NoiDung = uploadViewModel.NoiDung,
                    NoiDungSub = uploadViewModel.NoiDungSub,
                }
            };
            _newsService.AddNewNews(newsEntity);
            return Ok();
        }
        [HttpPost("UpdateBaiViet")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public ActionResult UpdateBaiViet(UpDateNewsViewModel upDateViewModel)
        {
            var newsEntity = new News()
            {
                Id = Convert.ToInt64(upDateViewModel.Id),
                TieuDe = upDateViewModel.TieuDe,
                TheLoai = upDateViewModel.TheLoai,
                Type = Convert.ToInt32(upDateViewModel.Type),
                UserId = Convert.ToInt64(upDateViewModel.UserId),
                NewsDetail = new NewsDetail()
                {
                    Id = Convert.ToInt64(upDateViewModel.NewDetailId),
                    NewsId = Convert.ToInt64(upDateViewModel.Id),
                    ImageUrl = upDateViewModel.ImageUrl,
                    ImageUrlSub = upDateViewModel.ImageUrlSub,
                    ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                    NoiDung = upDateViewModel.NoiDung,
                    NoiDungSub = upDateViewModel.NoiDungSub,
                }
            };
            try
            {
                _newsService.UpdateNews(newsEntity);
            }catch(Exception ex)
            {
                throw new ApiException(ex.Message);
            }
           
            return Ok();
        }
        [HttpPost("DeleteNew")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public ActionResult DeleteNew(long id)
        {
            try
            {
                _newsService.DeleteNews(id);
            }
            catch (Exception ex)
            {
                throw new ApiException(ex.Message);
            }
           
            return Ok();
        }
        [HttpGet("GetDetailNewForUpdate")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public ActionResult GetDetailNewForUpdate(long id)
        {
           
            var result = _newsService.GetNewsById(id);
            if(result == null)
            {
                throw new ApiException("Vui lòng chọn đúng bài viết");
            }
            var newsUpdate = new UpDateNewsViewModel()
            {
                Id = result.Id,
                NewDetailId = result.NewsDetail.Id,
                NoiDung = result.NewsDetail.NoiDung,
                NoiDungSub = result.NewsDetail.NoiDungSub,
                ThoiGianDang = result.NewsDetail.ThoiGianDang,
                TheLoai = result.TheLoai,
                TieuDe = result.TieuDe,
                UserId = result.UserId,
                Type = result.Type,
                ImageUrl = result.NewsDetail.ImageUrl,
                ImageUrlSub = result.NewsDetail.ImageUrlSub
            };
            return Ok(newsUpdate);
        }
        [HttpPost("Comment")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ActionResult Comment(CommentViewModel commentViewModel)
        {
            var comment = new Comment() {
                NewsId=Convert.ToInt64(commentViewModel.NewsId),
                UserId =Convert.ToInt64(commentViewModel.UserId),
                NoiDung = commentViewModel.NoiDung,
                UserName = commentViewModel.UserName
            };
            _newsService.AddComment(comment);
            var userId = _newsService.GetUserId(Convert.ToInt64(commentViewModel.NewsId));
            if (userId.Equals(commentViewModel.UserId))
            {
                return Ok();
            }
            else
            {
                var userSend = _userService.GetUserForNotification(Convert.ToInt64(commentViewModel.UserId));
                string body = userSend.Name + " đã bình luận bài viết của bạn với nội dung " + commentViewModel.NoiDung;
                _notificationService.SaveNotification(body, userId);
                var result = new NotificationViewModel();
                result.Body = userSend.Name + " đã bình luận bài viết của bạn";
                result.Title = "Thông báo";
                result.UserReceived = "user_" + userId;
                result.isClick = false;
                result.isRead = false;
                result.Link = "/chi-tiet/" + commentViewModel.NewsId;
                return Ok(result);
            }
            
        }
        [HttpGet("GetNotificationUser")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ActionResult GetNotificationUser(long id)
        {
           var notification = _notificationService.GetNotificationUser(id);
            return Ok(notification);
        }
    }
}