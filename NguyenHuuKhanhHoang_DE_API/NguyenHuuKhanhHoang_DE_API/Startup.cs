﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NguyenHuuKhanhHoang_DE_API.Auth;
using NguyenHuuKhanhHoang_DE_API.Controllers;
using NguyenHuuKhanhHoang_DE_API.CustomMiddleware;
using NguyenHuuKhanhHoang_DE_API.Data;
using NguyenHuuKhanhHoang_DE_API.JWTToken;
using NguyenHuuKhanhHoang_DE_API.Logging;
using NguyenHuuKhanhHoang_DE_API.Model.AuthModel;
using NguyenHuuKhanhHoang_DE_API.Service.Service.News;
using NguyenHuuKhanhHoang_DE_API.Service.Service.Users;
using FluentValidation.AspNetCore;
using FluentValidation;
using NguyenHuuKhanhHoang_DE_API.Model.LoginModel;
using NguyenHuuKhanhHoang_DE_API.Model.LoginModel.Validators;
using static NguyenHuuKhanhHoang_DE_API.Model.ErrorModel.ApiException;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.FileProviders;
using NguyenHuuKhanhHoang_DE_API.Model.NewsViewModel;
using NguyenHuuKhanhHoang_DE_API.Model.NewsViewModel.Validators;
using AutoMapper;
using Hangfire;
using Hangfire.SqlServer;
using NguyenHuuKhanhHoang_DE_API.BackgroundJob;
using Hangfire.Dashboard;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using NguyenHuuKhanhHoang_DE_API.Service.Service.Notification;
using Amazon.S3;
using NguyenHuuKhanhHoang_DE_API.Service.Service.AWS;

namespace NguyenHuuKhanhHoang_DE_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });
            var secretKey = Configuration.GetSection("Jwt").GetSection("SecretKey").Value;
            services.AddControllers().AddFluentValidation().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
            services.AddLocalization(options=>options.ResourcesPath="Resources");
            #region AWS Config
            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
            services.AddAWSService<IAmazonS3>();
            services.AddScoped<IAWSService, AWSService>();
            #endregion

            services.AddSingleton<IJwtFactory, JwtFactory>();
            services.AddSingleton<IJwtTokenHandler, JwtTokenHandler>();
            services.AddSingleton<ILoggerManager, LoggerManager>();
            services.AddDistributedMemoryCache();           // Đăng ký dịch vụ lưu cache trong bộ nhớ (Session sẽ sử dụng nó)
            services.AddSession(cfg => {                    // Đăng ký dịch vụ Session
                cfg.Cookie.Name = "NashTech";             // Đặt tên Session - tên này sử dụng ở Browser (Cookie)
                cfg.IdleTimeout = new TimeSpan(0, 60, 0);    // Thời gian tồn tại của Session
            });
            services.Configure<FormOptions>(options =>
            {
                options.MemoryBufferThreshold = Int32.MaxValue;
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "NashTech API ", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped(typeof(IRepository<>), typeof(EFRepository<>));
            services.AddScoped<ITestBackGroundJob, TestBackGroundJob>();
            services.AddDbContext<NashTechDbContext>(c => c.UseSqlServer(Configuration.GetConnectionString("MainConnection")));
            #region Config JWT Token
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

            // Configure JwtIssuerOptions
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            });
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(configureOptions =>
            {
                configureOptions.TokenValidationParameters = tokenValidationParameters;
            });
            #endregion
            services.AddAuthentication("Bearer");
            services.AddTransient<IPrincipal>(provider => provider.GetService<IHttpContextAccessor>().HttpContext?.User);
            services.AddTransient<IValidator<LoginModel>, LoginViewModelValidator>();
            services.AddTransient<IValidator<RegisterViewModel>, RegisterViewModelValidator>(); 
            services.AddTransient<IValidator<UpLoadNewsViewModel>, UpLoadNewsViewModelValidator>();
            services.AddTransient<IValidator<UpDateNewsViewModel>, UpDateNewsViewModelValidator>();
            services.AddHttpContextAccessor();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie();
            //services.AddAutoMapper(typeof(Profile));
            var typeMappingProfiles = Assembly.GetExecutingAssembly().GetTypes()
               .Where(type => type.BaseType == typeof(Profile));
            var mappingConfig = new MapperConfiguration(mc =>
            {
                foreach (var t in typeMappingProfiles)
                {
                    mc.AddProfile(t);
                }
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = (context) => throw new ValidationApiException(context.ModelState);
            });
            #region Hangfire
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"), new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }));
            // Add the processing server as IHostedService
            services.AddHangfireServer();
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IBackgroundJobClient backgroundJobs)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.FromFile(@"nashtech-firebase.json")
                });

            }
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(Directory.GetCurrentDirectory(), "Resources")),
                RequestPath = "/Resources"
            });
            

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "NashTech API");
            });
            app.UseCors("CorsPolicy");
            var supportedCultures = new[]
            {
                new CultureInfo("en-US"),
                new CultureInfo("fr")
            };
            app.UseSession();
            //app.UseHttpsRedirection();
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            });
            app.UseMiddleware<CustomExceptionMiddleware>();
            app.UseRouting();
           
            app.UseAuthentication();
            app.UseAuthorization();
            #region Configure Hangfire Dashboard
            var secretKey = Configuration.GetSection("Jwt").GetSection("SecretKey").Value;
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
            };
            var options = new DashboardOptions
            {
                Authorization = new IDashboardAuthorizationFilter[]
                {
                    new HangfireDashboardJwtAuthorizationFilter(tokenValidationParameters, "Admin")
                }
            };
            app.UseHangfireDashboard("/hangfire", options);
            HangfireScheduler.ConfigureRecurringJobs();
            #endregion

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
