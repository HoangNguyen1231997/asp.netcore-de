﻿using NguyenHuuKhanhHoang_DE_API.Model.AuthModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.JWTToken
{
   public interface IJwtFactory
    {
        AccessToken GenerateTokenJWT(long id, string userName, string Role);
    }
}
