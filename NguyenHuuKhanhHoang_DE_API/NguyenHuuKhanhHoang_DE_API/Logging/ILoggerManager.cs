﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Logging
{
    public interface ILoggerManager
    {
        
        void LogError(string message);
    }
}
