﻿using FluentValidation;
using NguyenHuuKhanhHoang_DE_API.Service.Service.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.LoginModel.Validators
{
    public class RegisterViewModelValidator : AbstractValidator<RegisterViewModel>
    {
        public RegisterViewModelValidator(IUserService userService)
        {
            RuleFor(x => x.UserName)
                   .NotEmpty().WithMessage("Tên đăng nhập yêu cầu nhập");
            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Mật khẩu yêu cầu nhập");
            RuleFor(x => x.ConfirmPassword)
                .NotEmpty().WithMessage("Xác nhận khẩu yêu cầu nhập");
            RuleFor(x => x.ConfirmPassword)
               .MustAsync(async (model, input, s) => await userService.CheckConfirmPass(model.Password, model.ConfirmPassword).ConfigureAwait(false)).When(x => x.Password != null && x.ConfirmPassword != null)
               .WithMessage("Xác nhận mật khẩu không khớp");
            RuleFor(x => x.UserName)
              .MustAsync(async (model, input, s) => await userService.CheckExistUser(model.UserName).ConfigureAwait(false)).When(x => x.UserName != null)
              .WithMessage("Tên đăng nhập đã tồn tại");
            RuleFor(x => x.GioiThieu)
                .NotEmpty().WithMessage("Giới thiệu yêu cầu nhập");

        }
    }
}

