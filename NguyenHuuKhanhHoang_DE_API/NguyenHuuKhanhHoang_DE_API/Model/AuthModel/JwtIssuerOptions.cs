﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.AuthModel
{
    public class JwtIssuerOptions
    {
        public DateTime Expiration => IssuedAt.Add(ValidFor);

        ///  "nbf" (Not Before) Claim - The "nbf" (not before) claim identifies the time before which the JWT MUST NOT be accepted for processing.
        public DateTime NotBefore => DateTime.Now;

        ///   "iat" (Issued At) Claim - The "iat" (issued at) claim identifies the time at which the JWT was issued.
        public DateTime IssuedAt => DateTime.Now;

        /// Set the timespan the token will be valid for (default is 120 min)
        public TimeSpan ValidFor { get; set; } = TimeSpan.FromMinutes(120);

        /// The signing key to use when generating tokens.
        public SigningCredentials SigningCredentials { get; set; }
    }
}
