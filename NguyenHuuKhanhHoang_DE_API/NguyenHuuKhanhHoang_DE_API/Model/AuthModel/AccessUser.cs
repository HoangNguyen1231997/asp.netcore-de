﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.AuthModel
{
    public class AccessUser
    {
        public AccessToken AccessToken { get; set; }
        public string UserName { get; set; }
        public long Id { get; set; }
    }
}
