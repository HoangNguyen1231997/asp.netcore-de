﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.AuthModel
{
    public class AccessToken
    {
        public long Id { get; }
        public string Token { get; }
        public int ExpiresIn { get; }

        public AccessToken(long id, string token, int expiresIn)
        {
            Id = id;
            Token = token;
            ExpiresIn = expiresIn;
        }
    }
}
