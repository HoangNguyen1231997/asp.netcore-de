﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.TestAutoMapper.News
{
    public class NewDetailViewModel
    {
        public long Id { get; set; }
        public long NewsId { get; set; }
        public string NoiDung { get; set; }
        public string NoiDungSub { get; set; }
        public string ThoiGianDang { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlSub { get; set; }
        public  NewsViewModel News { get; set; }
    }
}
