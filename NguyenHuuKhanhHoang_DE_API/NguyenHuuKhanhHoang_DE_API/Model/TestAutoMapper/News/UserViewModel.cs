﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.TestAutoMapper.News
{
    public class UserViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string GioiThieu { get; set; }
        public List<NewsViewModel> News { get; set; }
    }
}
