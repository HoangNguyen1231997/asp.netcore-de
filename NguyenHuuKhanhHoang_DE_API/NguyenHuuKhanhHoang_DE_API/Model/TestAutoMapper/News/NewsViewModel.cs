﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.TestAutoMapper.News
{
    public class NewsViewModel
    {
        public NewsViewModel()
        {
            //User = new UserViewModel();
            //NewsDetail = new NewDetailViewModel();
            //Comments = new List<CommentViewModel>();
        }
        public long Id { get; set; }
        public long UserId { get; set; }
        public string TieuDe { get; set; }
        public string TheLoai { get; set; }
        public int Type { get; set; }
        public UserViewModel User { get; set; }

        public NewDetailViewModel NewsDetail { get; set; }
        public List<CommentViewModel> Comments { get; set; }
    }
}
