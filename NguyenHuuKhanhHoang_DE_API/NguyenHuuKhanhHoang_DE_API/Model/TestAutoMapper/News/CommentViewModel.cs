﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.TestAutoMapper.News
{
    public class CommentViewModel
    {
        public long Id { get; set; }
        public string NoiDung { get; set; }
        public long NewsId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public NewsViewModel News { get; set; }
    }
}
