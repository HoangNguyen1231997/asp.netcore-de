﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.TestAutoMapper
{
    public class NewsMappingProfile:Profile
    {
        public NewsMappingProfile()
        {
            CreateMap<Core.Domain.Entity.News.News, Model.TestAutoMapper.News.NewsViewModel>().
            ForMember(x => x.NewsDetail, o => o.MapFrom(s => s.NewsDetail)).
            ForMember(x => x.User, o => o.MapFrom(s => s.User)).
             ForMember(x => x.Comments, o => o.MapFrom(s => s.Comments));
            //CreateMap<Model.TestAutoMapper.News.NewsViewModel, Core.Domain.Entity.News.News>().
            //    ForMember(x => x.NewsDetail, o => o.MapFrom(s => s.NewsDetail)).
            //     ForMember(x => x.User, o => o.MapFrom(s => s.User)).
            //      ForMember(x => x.Comments, o => o.MapFrom(s => s.Comments));
            CreateMap<Core.Domain.Entity.News.NewsDetail, Model.TestAutoMapper.News.NewDetailViewModel>();
            CreateMap<Core.Domain.Entity.Users.User, Model.TestAutoMapper.News.UserViewModel>();
            CreateMap<Core.Domain.Entity.Comments.Comment, Model.TestAutoMapper.News.CommentViewModel>();
        }
    }
}
