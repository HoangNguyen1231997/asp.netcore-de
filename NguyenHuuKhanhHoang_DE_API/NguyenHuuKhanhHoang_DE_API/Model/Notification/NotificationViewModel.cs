﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.Notification
{
    public class NotificationViewModel
    {
        public string Body { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string UserReceived { get; set; }
        public bool isRead { get; set; }
        public bool isClick { get; set; }
    }
}
