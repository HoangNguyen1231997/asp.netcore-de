﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.CommentViewModel
{
    public class CommentViewModel
    {
        public string NoiDung { get; set; }
        public string UserName { get; set; }
        public long? UserId { get; set; }
        public long? NewsId { get; set; }
    }
}
