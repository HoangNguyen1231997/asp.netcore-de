﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.NewsViewModel.Validators
{
    public class UpDateNewsViewModelValidator : AbstractValidator<UpDateNewsViewModel>
    {
        public UpDateNewsViewModelValidator()
        {
            RuleFor(x => x.TieuDe)
                .NotEmpty().WithMessage("Tiêu đề yêu cầu nhập");
            RuleFor(x => x.NoiDung)
                .NotEmpty().WithMessage("Nội dung yêu cầu nhập");
            RuleFor(x => x.NoiDungSub)
                .NotEmpty().WithMessage("Nội dung phụ yêu cầu nhập");
            RuleFor(x => x.TheLoai)
                .NotEmpty().WithMessage("Thể loại yêu cầu nhập");

        }
    }
}
