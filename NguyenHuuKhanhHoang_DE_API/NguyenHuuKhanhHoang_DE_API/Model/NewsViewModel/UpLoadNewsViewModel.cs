﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Model.NewsViewModel
{
    public class UpLoadNewsViewModel
    {
        public string TieuDe { get; set; }
        public long? UserId { get; set; }
        public string TheLoai { get; set; }
        public int? Type { get; set; }
        public string NoiDung { get; set; }
        public string NoiDungSub { get; set; }
        public string ThoiGianDang { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlSub { get; set; }
    }
}
