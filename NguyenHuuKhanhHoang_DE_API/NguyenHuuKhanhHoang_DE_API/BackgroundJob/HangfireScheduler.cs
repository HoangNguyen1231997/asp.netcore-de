﻿using Hangfire;
using Hangfire.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.BackgroundJob
{
    public class HangfireScheduler
    {
        public static void ConfigureRecurringJobs()
        {
            using (var connection = JobStorage.Current.GetConnection())
            {
                foreach (var recurringJob in StorageConnectionExtensions.GetRecurringJobs(connection))
                {
                    RecurringJob.RemoveIfExists(recurringJob.Id);
                }
            }
            //RecurringJob.AddOrUpdate<ITestBackGroundJob>(job => job.Run(), Cron.Minutely);
        }
    }
}
