﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.BackgroundJob
{
    public interface ITestBackGroundJob
    {
        //[DisableConcurrentExecution(60, Order = 2)]
        [AutomaticRetry(Attempts = 0)]
        void Run();
    }
}
