﻿using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.News;
using NguyenHuuKhanhHoang_DE_API.Service.Service.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.BackgroundJob
{
    public class TestBackGroundJob : ITestBackGroundJob
    {
        private INewsService _newsService;
        public TestBackGroundJob(INewsService newsService)
        {
            _newsService = newsService;
        }
        public void Run()
        {
            var newsEntity = new News()
            {
                TieuDe = "Test ne: "+DateTime.Now,
                TheLoai = "Test Background",
                Type = 2,
                UserId = 1,
                NewsDetail = new NewsDetail()
                {
                    ImageUrl = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                    ImageUrlSub = "https://www.nashtechglobal.com/wp-content/uploads/2018/11/Top-50-IT-company.jpg",
                    ThoiGianDang = DateTime.Now.ToString("MMM dd yyyy"),
                    NoiDung = "Test",
                    NoiDungSub = "Test",
                }
            };
            _newsService.AddNewNews(newsEntity);
        }
    }
}
