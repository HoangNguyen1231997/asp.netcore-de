﻿using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.Comments;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.News
{
    public interface INewsService
    {
        long GetUserId(long newsId);
        void UpdateNews(Core.Domain.Entity.News.News newsEntity);
        Core.Domain.Entity.News.News GetNewsById(long id);
        void AddComment(Comment commentEntity);
        Task<List<Core.Domain.Entity.News.News>> GetNewList(int typeNew);
        Task<Core.Domain.Entity.News.News> GetNewsDetailValue(long id);
        void AddNewNews(Core.Domain.Entity.News.News newsEntity);
        void DeleteNews(long id);
        Task<List<Core.Domain.Entity.News.News>> GetNewListByUser(long userId);
    }
}
