﻿using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.Users
{
    public interface IUserService
    {
        void SetFCMToken(long userId, string tokenFCM);
        User GetUserForNotification(long id);
        void CheckAndAddUserFCMToken(long userId, string tokenFCM);
        Task<User> GetDeatilUser(long id);
        Task<bool> CheckExistUser(string userName);
        Task<bool> CheckConfirmPass(string pass, string conFirmPass);
        User GetDeatilUserLogin(string name, string pass);
        User CheckUserExist(string name);
        void AddNewUser(User userEntity);
        void AddNewRole(UserRole userRoleEntity);
        string PasswordHash(string password);
    }
}
