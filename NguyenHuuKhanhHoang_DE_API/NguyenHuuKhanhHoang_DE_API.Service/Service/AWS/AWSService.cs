﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.S3.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.AWS;
using NguyenHuuKhanhHoang_DE_API.Data;
using NguyenHuuKhanhHoang_DE_API.Service.ValueObject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.AWS
{
    public class AWSService: IAWSService
    {
        private IRepository<TaiLieuDinhKem> _taiLieuDinhKemRepository;
        private static String accessKey = "AKIAIOU7HM4I3XUGD3ZA";
        private static String accessSecret = "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";
        private const string filePath = "Resources\\chuong.jpg";
        private const string filePath2 = "Resources\\chuongicon.png";
        private const string filePath3 = "Resources\\download.jfif";
        private const string filePath4 = "Resources\\img_forest.jpg";
        private const string UploadWithKeyName = "UploadWithKeyName";
        private const string FileStreamUpload = "FileStreamUpload";
        private const string AdvancedUpload = "AdvancedUpload";
        private IAmazonS3 _amazonS3;
        public AWSService(IRepository<TaiLieuDinhKem> taiLieuDinhKemRepository)
        {
            _taiLieuDinhKemRepository = taiLieuDinhKemRepository;
            _amazonS3 = new AmazonS3Client(accessKey, accessSecret, Amazon.RegionEndpoint.APSoutheast1);
        }
        public async Task<AWSResponseVO> CreateBucketAmz(string name)
        {
            try
            {
                if(await AmazonS3Util.DoesS3BucketExistAsync(_amazonS3, name) == false)
                {
                    var putBucketrequest = new PutBucketRequest
                    {
                        BucketName = name,
                        UseClientRegion = true
                    };
                    var response = await _amazonS3.PutBucketAsync(putBucketrequest);
                    return new AWSResponseVO()
                    {
                        Status = response.HttpStatusCode,
                        Message = response.ResponseMetadata.RequestId
                    };
                }
                return new AWSResponseVO()
                {
                    Status = HttpStatusCode.InternalServerError,
                    Message = "Bucket nay đã tồn tại rồi"
                };
            }
            catch (AmazonS3Exception ex)
            {
                return new AWSResponseVO()
                {
                    Status = ex.StatusCode,
                    Message = ex.Message
                };
            }
            catch(Exception e)
            {
                return new AWSResponseVO()
                {
                    Status = HttpStatusCode.InternalServerError,
                    Message = e.Message
                };
            }
        }
        public async Task<bool> CreateFolderBucketAsync(string folderPath)
        {
            if (!(await DoesFolderExistsAsync(folderPath)))
            {
                var putObjectResponse = await _amazonS3.PutObjectAsync(
                    new PutObjectRequest()
                    {
                        BucketName = "khanhhoangtestaws",
                        Key = folderPath + "/",
                        ContentType = null,
                        StorageClass = S3StorageClass.Standard
                    }
                );
                if (putObjectResponse.HttpStatusCode == HttpStatusCode.OK)
                    return true;
            }
            return false;
            //create
        }
        private async Task<bool> DoesFolderExistsAsync(string folderPath)
        {
            ListObjectsV2Request request = new ListObjectsV2Request();
            request.BucketName = "khanhhoangtestaws";
            request.Prefix = folderPath + "/";
            request.MaxKeys = 1;
            ListObjectsV2Response response = await _amazonS3.ListObjectsV2Async(request);
            return response.S3Objects.Any();
        }
        public async Task UploadFileAsync(string name)
        {
            try
            {
                var fileTranferUtil = new TransferUtility(_amazonS3);
                //option1
                await fileTranferUtil.UploadAsync(filePath, name);
                //option2
                await fileTranferUtil.UploadAsync(filePath2, name, UploadWithKeyName);
                //option3
                using (var fileTourUpload = new FileStream(filePath3, FileMode.Open, FileAccess.Read))
                {
                    await fileTranferUtil.UploadAsync(filePath3, name, FileStreamUpload);
                };
                //option4
                var fileTranferUti = new TransferUtilityUploadRequest
                {
                    BucketName = name,
                    FilePath = filePath4,
                    StorageClass = S3StorageClass.Standard,
                    PartSize = 6291456,
                    CannedACL = S3CannedACL.NoACL
                };
                fileTranferUti.Metadata.Add("param1", "Value1");
                fileTranferUti.Metadata.Add("param2", "Value2");
                await fileTranferUtil.UploadAsync(fileTranferUti);
            }
            catch (AmazonS3Exception e)
            {

            }
            catch (Exception ex) { 
            }
            
        }
        public void GetPreSignedUploadFileUrl(string name)
        {
            var key = $"{Guid.NewGuid()}.{"jpg"}";
            var url = _amazonS3.GetPreSignedURL(new GetPreSignedUrlRequest()
            {
                BucketName = name,
                Expires = DateTime.UtcNow.AddDays(1).ToLocalTime(),
                Key = key,
                Headers =
                    {
                        ContentType = "image/jpeg", // "application/x-www-form-urlencoded; charset=UTF-8",
						ContentLength = 6291456
                    },
                ContentType = "image/jpeg",
                Verb = HttpVerb.PUT,
            });

        }
        public async Task TestUploadFileByIFormFile(IFormFile file, string bucketName, string folder,long userId)
        {
            var typeFile = file.FileName.Substring(file.FileName.IndexOf('.')+1, file.FileName.Length-1-file.FileName.IndexOf('.'));
            var keyName = folder != null ? folder + "/" + Guid.NewGuid().ToString() + "_" + file.FileName : Guid.NewGuid().ToString() + "_" + file.FileName;
            var taiLieu = new TaiLieuDinhKem()
            {
                BucketName = bucketName,
                CreateOn= DateTime.Now,
                FolderName = folder,
                KeyName= keyName,
                NameFile=file.FileName,
                UserId= userId,
                TypeFile= typeFile

            };
            _taiLieuDinhKemRepository.Add(taiLieu);
           var _amazonS31 = new AmazonS3Client(accessKey, accessSecret, Amazon.RegionEndpoint.USEast2);
            //using (var newMemoryStream = new MemoryStream())
            //{
            //    file.CopyTo(newMemoryStream);

            //    var uploadRequest = new TransferUtilityUploadRequest
            //    {
            //        InputStream = newMemoryStream,
            //        Key = file.FileName,
            //        BucketName = "hoangtest1231997",
            //        CannedACL = S3CannedACL.NoACL
            //    };

            //    var fileTransferUtility = new TransferUtility(_amazonS3);
            //    await fileTransferUtility.UploadAsync(uploadRequest);
            //}
            try
            {
                var fs = file.OpenReadStream();
                var request = new Amazon.S3.Model.PutObjectRequest
                {
                    BucketName = "khanhhoangtestaws",
                    Key = keyName,
                    InputStream = fs,
                    ContentType = file.ContentType,
                    CannedACL = S3CannedACL.PublicRead
                };
                var result = await _amazonS3.PutObjectAsync(request);
              
            }
            catch(Exception ex)
            {

            }
            
         }
        public async Task GetObjectFromS3(string name,string keyName)
        {
            var taiLieu = await GetTaiLieuInDataBase(name, keyName);
            try
            {
                var request = new GetObjectRequest
                {
                    BucketName = name,
                    Key = keyName
                };
                string filePath = "D:\\"+taiLieu.NameFile ;
                using (var getObjectResponse = await _amazonS3.GetObjectAsync(request).ConfigureAwait(false))
                {
                    await getObjectResponse.WriteResponseStreamToFileAsync(filePath, false, default(System.Threading.CancellationToken)).ConfigureAwait(false);
                }
               
            }
            catch(AmazonS3Exception e)
            {
                
            }
            catch(Exception ex)
            {

            }
        }
        public string GetTaiLieuUrl(string bucketName, string key)
        {
            return _amazonS3.GeneratePreSignedURL(bucketName, key, DateTime.UtcNow.AddMinutes(60).ToLocalTime(), null);
        }
        public async Task<TaiLieuDinhKem> GetTaiLieuInDataBase(string bucketName , string keyName)
        {
            var taiLieu = await _taiLieuDinhKemRepository.TableNoTracking.Where(x => x.BucketName == bucketName && x.KeyName == keyName).FirstOrDefaultAsync();
            return taiLieu;
        }
        public async Task<AWSResponseVO> DeleteBucketAsync(string bucketName)
        {

            try
            {
                if (await AmazonS3Util.DoesS3BucketExistAsync(_amazonS3, bucketName))
                {
                    // Delete the bucket and return the response
                    var response = await _amazonS3.DeleteBucketAsync(bucketName);
                    return new AWSResponseVO
                    {
                        Message = response.ResponseMetadata.RequestId,
                        Status = response.HttpStatusCode
                    };
                }
            }

            // Catch specific amazon errors
            catch (AmazonS3Exception e)
            {
                return new AWSResponseVO
                {
                    Message = e.Message,
                    Status = e.StatusCode
                };
            }

            // Catch other errors
            catch (Exception e)
            {
                return new AWSResponseVO
                {
                    Message = e.Message,
                    Status = HttpStatusCode.InternalServerError
                };
            }

            return new AWSResponseVO
            {
                Message = "Something went wrong",
                Status = HttpStatusCode.InternalServerError
            };
        }
        public async Task<AWSResponseVO> DeleteObjectFromS3Async(string bucketName, string keyName)
        {
           
            if (string.IsNullOrEmpty(keyName)) keyName = "test.txt";
            var taiLieu = await GetTaiLieuInDataBase(bucketName, keyName);
            try
            {
                // Build the request with the bucket name and the keyName (name of the file)
                var request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName
                };

                await _amazonS3.GetObjectAsync(request);

                await _amazonS3.DeleteObjectAsync(bucketName, keyName);
                _taiLieuDinhKemRepository.Delete(taiLieu);
                return new AWSResponseVO
                {
                    Message = "The file was successfully deleted",
                    Status = HttpStatusCode.OK
                };
            }

            // Catch specific amazon errors
            catch (AmazonS3Exception e)
            {
                return new AWSResponseVO
                {
                    Message = e.Message,
                    Status = e.StatusCode
                };
            }

            // Catch other errors
            catch (Exception e)
            {
                return new AWSResponseVO
                {
                    Message = e.Message,
                    Status = HttpStatusCode.InternalServerError
                };
            }
        }
    }
}
