﻿using Microsoft.AspNetCore.Http;
using NguyenHuuKhanhHoang_DE_API.Service.ValueObject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.AWS
{
    public interface IAWSService
    {
        Task<AWSResponseVO> CreateBucketAmz(string name);
        Task<bool> CreateFolderBucketAsync(string folderPath);
        Task UploadFileAsync(string name);
        void GetPreSignedUploadFileUrl(string name);
        Task TestUploadFileByIFormFile(IFormFile file, string bucketName, string folder, long userId);
        Task GetObjectFromS3(string name,string keyName);
        string GetTaiLieuUrl(string bucketName, string key);
        Task<AWSResponseVO> DeleteBucketAsync(string bucketName);
        Task<AWSResponseVO> DeleteObjectFromS3Async(string bucketName, string keyName);
    }
}
