﻿using FirebaseAdmin.Messaging;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.UserFCM;
using NguyenHuuKhanhHoang_DE_API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.Notification
{
    public class NotificationService: INotificationService
    {
        public IRepository<UserFCMToken> _userFCMTokenRepository;
        public IRepository<UserNotification> _userNotificationRepository;
        public NotificationService(IRepository<UserFCMToken> userFCMTokenRepository, IRepository<UserNotification> userNotificationRepository)
        {
            _userFCMTokenRepository = userFCMTokenRepository;
            _userNotificationRepository = userNotificationRepository;
        }
        public async Task SaveNotification(string body , long userId)
        {
            var userFCMToken = _userFCMTokenRepository.TableNoTracking.Where(x => x.UserId == userId && x.IsSigning == true).ToList();
            if(userFCMToken.Count > 0)
            {
                var messageNotification = new FirebaseAdmin.Messaging.AndroidNotification
                {
                    //Body = body,
                    //Title = "Thông báo",
                    ClickAction = "http://localhost:4200/home"
                };
                var messageNotification1 = new FirebaseAdmin.Messaging.Notification
                {
                    Body = body,
                    Title = "Thông báo"
                };
                var message = new Message
                {
                    
                    Apns = new ApnsConfig
                    {
                        
                        Aps = new Aps
                        {
                            // CriticalSound= messageAction12,

                            Sound = "https://localhost:5001/Resources/point-blank"
                            // Sound = url
                            //Sound = "default"
                        }
                    },
                    Android  = new AndroidConfig { 
                        Notification = messageNotification
                    },
                     Notification = messageNotification1,
                    Data = new Dictionary<string, string>()
                     {
                        { "user_id", "1" },
                        { "icon", "https://localhost:5001/Resources/ring.png" }
                    },
                    Webpush = new WebpushConfig
                    {
                        Notification = new WebpushNotification
                        {
                            Icon = "https://localhost:5001/Resources/chuongicon.png",
                        },

                        //FcmOptions = new WebpushFcmOptions
                        //{
                        //    Link = "http://localhost:4200/home"
                        //}
                    },
                    //Token = "d4Fzozz5pPZu0mEPYiF2n9:APA91bFLon-MgX1TMb9ckKJwOpG49ws1T6LM_w5vlL1HfvdJaxdtppaAq9krM4l29eJOgrpOz5XrrWwUPR0NqFF0715ivN_dE2zVbmbnyC761eyAxMZ8GDDdPAZCuHjOVnlgLyWKLQPh"
                    // Topic ="Test"
                };
                foreach (var item in userFCMToken)
                {
                    _userNotificationRepository.Add(
                        new UserNotification()
                        {
                            UserId = userId,
                            TokenFCM = item.TokenFCM,
                            Message = body,
                            CreateOn = DateTime.Now,
                            IsRead =false
                        });
                    message.Token = item.TokenFCM;
                    string response = await FirebaseMessaging.DefaultInstance.SendAsync(message);
                }
                //string response = await FirebaseMessaging.DefaultInstance.SendAsync(message);
            }
            else
            {
                _userNotificationRepository.Add(
                       new UserNotification()
                       {
                           UserId = userId,
                           Message = body
                       });
            }
        }
        public List<UserNotification> GetNotificationUser(long id)
        {
            return _userNotificationRepository.TableNoTracking.Where(x => x.UserId == id).ToList();
        }
        private async Task<bool> SubscribeToTopicAsync(string topic, params string[] registrationTokens)
        {
            try
            {
                var response = await FirebaseMessaging.DefaultInstance.SubscribeToTopicAsync(registrationTokens, topic);
                return response.SuccessCount == registrationTokens.Length;
            }
            catch
            {
                return false;
            }
        }
    }
}
