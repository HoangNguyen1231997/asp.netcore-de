﻿using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.UserFCM;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NguyenHuuKhanhHoang_DE_API.Service.Service.Notification
{
    public interface INotificationService
    {
        Task SaveNotification(string body, long userId);
        List<UserNotification> GetNotificationUser(long id);
    }
}
