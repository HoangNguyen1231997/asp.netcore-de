﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace NguyenHuuKhanhHoang_DE_API.Service.ValueObject
{
    public class AWSResponseVO
    {
        public string Message { get; set; }
        public HttpStatusCode Status { get; set; }
    }
}
