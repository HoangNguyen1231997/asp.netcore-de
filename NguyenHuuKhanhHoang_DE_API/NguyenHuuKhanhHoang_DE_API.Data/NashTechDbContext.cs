﻿using Microsoft.EntityFrameworkCore;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.AWS;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.Comments;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.News;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.UserFCM;
using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace NguyenHuuKhanhHoang_DE_API.Data
{
    public class NashTechDbContext : DbContext
    {
        public NashTechDbContext(DbContextOptions<NashTechDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        public DbSet<UserFCMToken> UserFCMTokens { get; set; }
        public DbSet<TaiLieuDinhKem> TaiLieuDinhKems { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<NewsDetail> NewsDetails { get; set; }
        public DbSet<UserNotification> UserNotifications{ get; set; }
        public DbSet<Comment> Comments { get; set; }
       
    }
}
