﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NguyenHuuKhanhHoang_DE_API.Data.Migrations
{
    public partial class UpdateAWSS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreateOn",
                table: "TaiLieuDinhKem",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FolderName",
                table: "TaiLieuDinhKem",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateOn",
                table: "TaiLieuDinhKem");

            migrationBuilder.DropColumn(
                name: "FolderName",
                table: "TaiLieuDinhKem");
        }
    }
}
