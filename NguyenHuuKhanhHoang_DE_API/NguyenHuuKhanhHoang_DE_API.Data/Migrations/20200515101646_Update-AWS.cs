﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NguyenHuuKhanhHoang_DE_API.Data.Migrations
{
    public partial class UpdateAWS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaiLieuDinhKem",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BucketName = table.Column<string>(nullable: false),
                    KeyName = table.Column<string>(nullable: true),
                    TypeFile = table.Column<string>(nullable: true),
                    NameFile = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaiLieuDinhKem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaiLieuDinhKem_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaiLieuDinhKem_UserId",
                table: "TaiLieuDinhKem",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaiLieuDinhKem");
        }
    }
}
