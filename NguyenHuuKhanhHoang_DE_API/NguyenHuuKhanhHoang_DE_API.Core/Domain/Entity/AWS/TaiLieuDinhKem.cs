﻿using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.AWS
{
    [Table("TaiLieuDinhKem")]
    public class TaiLieuDinhKem
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string BucketName { get; set; }
        public string FolderName { get; set; }
        public string KeyName { get; set; }
        public string TypeFile { get; set; }
        public string NameFile { get; set; }
        public DateTime CreateOn { get; set; }
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
