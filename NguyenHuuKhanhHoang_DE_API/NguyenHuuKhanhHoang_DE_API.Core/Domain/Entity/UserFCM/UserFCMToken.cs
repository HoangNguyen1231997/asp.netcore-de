﻿using NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NguyenHuuKhanhHoang_DE_API.Core.Domain.Entity.UserFCM
{
    [Table("UserFCMToken")]
    public class UserFCMToken
    {
        [Key]
        public long Id { get; set; }
        public long UserId { get; set; }
        [Required]
        public string TokenFCM { get; set; }
        [Required]
        public bool IsSigning { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
    
}
