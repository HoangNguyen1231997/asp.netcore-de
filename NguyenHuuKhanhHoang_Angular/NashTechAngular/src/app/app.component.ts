import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';
import { MessagingService } from 'src/app/service/messaging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'NashTechAngular';
  message;
userName:string;
  constructor(private toastr:ToastrService,private authService:AuthService,private messagingService: MessagingService){}

  ngOnInit(): void {
    this.GetNameUser();
    // this.messagingService.requestPermission()
    // this.messagingService.onMessageReceived()
    // this.messagingService.currentMessage.subscribe(data => {
     
    //   console.log("Dataaaaa",data)
    // })
  }
  GetNameUser(){
    let user = this.authService.getAccessUser();
    if(user!=null && user!= undefined){
      this.userName = user.userName;
    }
     
  }
  testThu(){
    this.toastr.error("Test","Error")
  }
}
