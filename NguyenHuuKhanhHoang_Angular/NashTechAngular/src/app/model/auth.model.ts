export class Auth {

}
export interface AccessUser {
  accessToken: AccessToken;
  userName: string;
  id: number;

}
export interface AccessToken {
  Id: number;
  token: string;
  expiresIn: number;
}
