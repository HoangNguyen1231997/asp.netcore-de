export class Login {
  constructor(
    public UserName: string = null,
    public Password: string = null,
    public FCMToken: string = null
  ) { }
}
export class Register {
  constructor(
    public UserName: string = null,
    public Password: string = null,
    public ConfirmPassword: string = null,
    public GioiThieu: string = null,
  ) { }
}
