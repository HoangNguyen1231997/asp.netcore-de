import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeModule } from 'src/app/main/home/home.module';
import { RegisterComponent } from './register/register.component';
@NgModule({
 
  imports: [
    CommonModule,
    FormsModule,
    HomeModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    
  ],
  declarations: [LoginComponent, RegisterComponent]
})
export class LoginModule { }
