import { Component, OnInit } from '@angular/core';
import { Login } from 'src/app/main/login/login.model';
import { ApiService } from 'src/app/service/api.service';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MessagingService } from 'src/app/service/messaging.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userLogin: Login;
  messUser: string = null;
  messPass: string = null;
  constructor(private router: Router, private messagingService: MessagingService,private apiService: ApiService, private authService: AuthService, private toastr: ToastrService) { }

  ngOnInit(): void {

    this.userLogin = new Login();
    this.messagingService.requestPermission()
  }
  Login() {
    this.messPass =null;
    this.messUser=null;
    this.userLogin.FCMToken = this.messagingService.getFcmToken();
    this.apiService.post<any>("Login/Login", this.userLogin)
      .subscribe(result => {
        this.authService.setAuth(result);
        this.router.navigate(['/home']);
        this.toastr.success("Đăng nhập thành công", "Thành công")
      },
      (err: any) => {
        if (err.error.Errors != undefined) {
          err.error.Errors.forEach(element => {
            if (element.Field == "Password") {
              this.messPass = element.Message;
            }
            if (element.Field == "UserName") {
              this.messUser = element.Message;
            }
          });
          err.error.Message = "Đăng nhập thất bại"
        }
        this.toastr.error(err.error.Message, "Lỗi")
      });
  }
  FireBase() {
    this.messPass =null;
    this.messUser=null;
    this.apiService.get<any>("Home/TestFireBase")
      .subscribe(result => {
       console.log("Test FireBase: ",result)
      },
      (err: any) => {
       
        console.log("Lỗi: ",err)
      });
  }
  onKey(event: any) {
    if (event.keyCode === 13) {
      this.Login();
    }
  }
}
