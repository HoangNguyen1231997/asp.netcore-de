import { Component, OnInit } from '@angular/core';
import { Register } from 'src/app/main/login/login.model';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { AuthService } from 'src/app/service/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userNameValidate:string=null
  passwordValidate:string =null
  confirmPassValidate:string = null
  gioiThieuValidate:string =null
  constructor(private router: Router, private apiService: ApiService, private authService: AuthService, private toastr: ToastrService) { }
  dangKy:Register;
  ngOnInit(): void {
    this.dangKy = new Register();
  }
  onKey(event: any) {
    if (event.keyCode === 13) {
      this.Register();
    }
  }
  Register() {
   this.userNameValidate=null;
   this.passwordValidate=null;
   this.confirmPassValidate=null;
   this.gioiThieuValidate=null;

    this.apiService.post<any>("Login/Register", this.dangKy)
      .subscribe(result => {
        this.router.navigate(['/login']);
        this.toastr.success("Đăng ký thành công", "Thành công")
      },
      (err: any) => {
        console.log("Loi ne",err)
        if (err.error.Errors != undefined) {
          err.error.Errors.forEach(element => {
            if (element.Field == "Password") {
              this.passwordValidate = element.Message;
            }
            if (element.Field == "UserName") {
              this.userNameValidate = element.Message;
            }
            if (element.Field == "ConfirmPassword") {
              this.confirmPassValidate = element.Message;
            }
            if (element.Field == "GioiThieu") {
              this.gioiThieuValidate = element.Message;
            }
          });
          err.error.Message = "Đăng ký thất bại"
        }
        this.toastr.error(err.error.Message, "Lỗi")
      });
  }

}
