import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/main/home/home.model';
import { AuthService } from 'src/app/service/auth.service';
import { ApiService } from 'src/app/service/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-thong-tin-user',
  templateUrl: './thong-tin-user.component.html',
  styleUrls: ['./thong-tin-user.component.css']
})
export class ThongTinUserComponent implements OnInit {
  user: User;
  userId: number = 0;
  pageNumber: number = 1;
  totalRecords: number;
  constructor(private authService: AuthService, private apiService: ApiService, private toastr: ToastrService, private router: Router) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.GetNameUser();
  }
  GetNameUser() {
    let user = this.authService.getAccessUser();
    if (user != null && user != undefined) {
      this.userId = user.id;
      this.GetDetail(user.id)
    } else {
      this.router.navigate(['/home']);
      this.toastr.error("Hết phiên làm việc vui lòng đăng nhập lại", "Lỗi")
    }

  }
  DeleteNew(id: number) {
    this.apiService.post<any>("Home/DeleteNew?id=" + id).subscribe(
      resultData => {
        this.router.navigate(['/home']);
        this.toastr.success("Xóa bài viết thành công", "Thông báo")
      }
      , (err: any) => {
        window.location.reload();
      });
  }
  GetDetail(id: number) {
    this.apiService.get<any>("Home/GetDetailUser?id=" + id).subscribe(
      resultData => {

        this.user = resultData;
        this.totalRecords = resultData.news.length;
        console.log(resultData)
      }
      , (err: any) => {

      });
  }
}
