import { Component, OnInit, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/service/api.service';
import { MessagingService } from 'src/app/service/messaging.service';
import { FireBaseDatabaseService } from 'src/app/service/fire-base-database.service';
import { FireBaseTestValue } from 'src/app/main/home/home.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService,private router: Router,private fireBaseService:FireBaseDatabaseService,private cdRef:ChangeDetectorRef,private messagingService:MessagingService, private toastr: ToastrService, private apiService: ApiService) { }
  userName: string
  notification: Array<FireBaseTestValue>;
  numberCheck:number;
  notificationNotClick: Array<FireBaseTestValue>;
  notificationDisplay:Array<FireBaseTestValue>;
  dataNotification:any;
  UserInfomation:any;
  countNotification:number =0;
  ngOnInit(): void {
    this.GetNameUser();
     this.messagingService.onMessageReceived()
    this.messagingService.currentMessage.subscribe(data => {
     
      console.log("Dataaaaa",data)
     // this.GetNotification(this.UserInfomation.id)
    })
    this.messagingService.listenToNotifications().subscribe(x=>console.log("Listen: ",x));
  }
  logout() {
    let token = this.messagingService.getFcmToken();
    this.apiService.setTokenFCMUser(this.UserInfomation.id,token)
    this.authService.logout();
    this.messagingService.deleteToken();
  }
  GetNameUser() {
   
    let user = this.authService.getAccessUser();
    if (user != null && user != undefined) {
      this.Check(user);
      this.UserInfomation = user;
      this.fireBaseService.getData('/message/user_'+user.id).forEach(element => {
        this.notification = new Array<FireBaseTestValue>();
        element.forEach(a=>{
          this.dataNotification = a.payload.val();
          this.dataNotification.key = a.key
          this.notification.push(this.dataNotification);
          console.log("a",a)
        })
        this.notificationNotClick = [...this.notification.filter(element=>element.isClick ==false)]
        this.notificationDisplay =  [...this.notification.filter((u, i) => i < 10)]
        this.countNotification = this.notificationNotClick.length;
        this.numberCheck = this.notification.length;
        console.log("Dataaaaaa",this.notificationNotClick)
      });
    }
  }
  HaveReadNotification(data:any){
    data.isRead =true;
    this.fireBaseService.updateData(data,'/message/user_'+this.UserInfomation.id+'/'+data.key);
    this.router.navigate([data.link]);
  }
  XemThem(){
    console.log("Co chay vao xem them")
    let takeValue = 10 + this.notificationDisplay.length;
    this.notificationDisplay = [...this.notification.filter((u, i) => i < takeValue)]
  }
  Check(user: any) {
    let token = this.messagingService.getFcmToken();
    this.apiService.get<any>("Home/CheckToken")
      .subscribe(result => {
        this.userName = user.userName;
      },
      (err: any) => {
        this.apiService.setTokenFCMUser(user.id,token)
        this.authService.logout();
        this.messagingService.deleteToken();
        window.location.reload();
        this.toastr.error("Phiên làm việc kết thúc vui lòng đăng nhập lại", "Thông báo")
        console.log("That bai", err)
      });
  }
  HaveClick(){
    if(this.countNotification >0){
      this.notification.forEach(element =>{
        element.isClick =true;
        this.fireBaseService.updateData(element,'/message/user_'+this.UserInfomation.id+'/'+element.key);
      })
    }
   
    
  }
  ngOnChanges(changes: SimpleChange) {
    console.log("Co vao",changes)
    // only run when property "data" changed
    if (changes['countNotification'] && changes['countNotification'].currentValue != changes['countNotification'].previousValue && changes['countNotification'].previousValue != undefined) {
      this.ngOnInit();
      console.log("Co vao")
    }
  }
  GetNotification(id: any) {
   
    this.apiService.get<Array<any>>("Home/GetNotificationUser?id="+id)
      .subscribe(result => {
        console.log("Notification: ",result)
        this.countNotification = result.length;
        this.cdRef.detectChanges();
        console.log("countNotification: ",this.countNotification)
      },
      (err: any) => {
       
      });
  }
  Click(){
    this.countNotification = this.countNotification+1;
  }
}
