import { Component, OnInit } from '@angular/core';
import { UploadNews, UpdateNews } from 'src/app/main/home/home.model';
import { ApiService } from 'src/app/service/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-sua-bai-viet',
  templateUrl: './sua-bai-viet.component.html',
  styleUrls: ['./sua-bai-viet.component.css']
})
export class SuaBaiVietComponent implements OnInit {

  constructor(private apiService: ApiService, private authService: AuthService, private route: ActivatedRoute, private router: Router, private toastr: ToastrService) { }
  updateNew: UpdateNews;
  userName: string = null
  fileupLoad: File = null
  newsId: number = 0;
  fileanhPhuupLoad: File = null
  disableAnhChinh: boolean = true;
  disableAnhPhu: boolean = true;
  tieuDeValidate: string = null;
  noiDungValidate: string = null;
  noiDungSubVaildate: string = null;
  theLoaiValidate: string = null;
  ngOnInit(): void {
    this.updateNew = new UpdateNews();
    const id: number = this.route.snapshot.params.id;
    this.newsId = id;
    this.GetNameUser();
    this.GetDetailNewForUpdate(id);
  }
  GetNameUser() {
    let user = this.authService.getAccessUser();
    if (user != null && user != undefined) {
      this.userName = user.userName;
    }

  }
  clickAnhChinh() {
    this.disableAnhChinh = false;
  }
  clickAnhPhu() {
    this.disableAnhPhu = false;
  }
  fileSelected(event) {
    this.fileupLoad = event.item(0);
    this.UploadAnhChinh();
  }
  fileanhPhuSelected(event) {
    this.fileanhPhuupLoad = event.item(0);
    this.UploadAnhPhu();
  }
  UploadAnhChinh() {
    this.apiService.postFile(this.fileupLoad.name, this.fileupLoad).subscribe(
      data => {
        this.updateNew.imageUrl = data.value;
        console.log("Ne:", data, this.updateNew)
      }, (err: any) => {
        console.log("Anh chinh loi", err)
      }
    );
  }
  UploadAnhPhu() {
    this.apiService.postFile(this.fileanhPhuupLoad.name, this.fileanhPhuupLoad).subscribe(
      data => {
        this.updateNew.imageUrlSub = data.value;
      }
    );
  }
  GetDetailNewForUpdate(id: number) {
    this.apiService.get<any>("Home/GetDetailNewForUpdate?id=" + id).subscribe(
      resultData => {
        this.updateNew = resultData;
        console.log("data", resultData)
      }, (err: any) => {
        this.router.navigate(['/home']);
        if(err.error !=null ||err.error !=undefined){
         
          this.toastr.error(err.error.Message, "Lỗi")
        }else{
          this.toastr.error("Phiên làm việc hết hạn vui lòng đăng nhập lại", "Lỗi")
        }
       
        
      });
  }
  UpdateNews() {
    this.noiDungSubVaildate = null;
    this.noiDungValidate = null;
    this.tieuDeValidate = null;
    this.theLoaiValidate = null;
    this.apiService.post<any>("Home/UpdateBaiViet", this.updateNew).subscribe(
      resultData => {
        this.router.navigate(['/chi-tiet/' + this.newsId]);
        this.toastr.success("Sửa bài viết thành công", "Thành công")
      }, (err: any) => {
        if (err.error.Errors != undefined) {
          err.error.Errors.forEach(element => {
            if (element.Field == "TieuDe") {
              this.tieuDeValidate = element.Message;
            }
            if (element.Field == "NoiDung") {
              this.noiDungValidate = element.Message;
            }
            if (element.Field == "NoiDungSub") {
              this.noiDungSubVaildate = element.Message;
            }
            if (element.Field == "TheLoai") {
              this.theLoaiValidate = element.Message;
            }
          });
          err.error.Message = "Sửa bài thất bại"
        } else {
          window.location.reload();
        }
        this.toastr.error(err.error.Message, "Lỗi")
      });
  }
}
