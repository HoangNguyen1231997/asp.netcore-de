import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuaBaiVietComponent } from './sua-bai-viet.component';

describe('SuaBaiVietComponent', () => {
  let component: SuaBaiVietComponent;
  let fixture: ComponentFixture<SuaBaiVietComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuaBaiVietComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuaBaiVietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
