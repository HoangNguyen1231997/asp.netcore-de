import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { News, Comments } from 'src/app/main/home/home.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';
import { MessagingService } from 'src/app/service/messaging.service';
import { FireBaseDatabaseService } from 'src/app/service/fire-base-database.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.css']
})
export class NewsDetailComponent implements OnInit {
id:number;
  constructor(private apiService: ApiService,private fireBaseService:FireBaseDatabaseService,private messagingService:MessagingService, private route: ActivatedRoute, private router: Router,
    private toastr: ToastrService, private authService: AuthService) { }
  binhLuan: Comments;
  detailNews: News;
  userId: number = 0;
  ngOnInit(): void {
    this.binhLuan = new Comments();
    this.detailNews = new News();
    this.route.paramMap.subscribe((data:any)=>{
      this.id = data.get('id');
      this.GetDetail(this.id);
      this.binhLuan.newsId = this.id;
      this.GetDetailAutoMapper(this.id);
    })
    //const id: number = this.route.snapshot.params.id;
    
  }
  GetDetail(id: number) {
    this.apiService.get<News>("Home/GetDetailNews?id=" + id).subscribe(
      resultData => {

        this.detailNews = resultData;
        console.log("Data", this.detailNews)
      }, (err: any) => {
        if(err.error !=null ||err.error !=undefined){
          this.router.navigate(['/home']);
          this.toastr.error(err.error.Message, "Lỗi")
        }else  {
          this.router.navigate(['/login']);
          this.toastr.error("Bạn cần đăng nhập trước khi đọc nội dung", "Lỗi")
        }
        console.log("LOiooioioioi", err)
      });
  }
  GetDetailAutoMapper(id: number) {
    this.apiService.get<News>("Home/GetDetailNewsTestAutoMapper?id=" + id).subscribe(
      resultData => {

      
        console.log("Test AutoMapper: ", this.detailNews)
      }, (err: any) => {
       
        console.log("LOiooioioioi", err)
      });
  }
  BinhLuan() {
    if (this.binhLuan.noiDung != null && this.binhLuan.noiDung != undefined) {
      
      this.Dang();
      this.binhLuan.noiDung == undefined
    } else {
      this.toastr.error("Vui lòng nhập bình luận trước khi đăng", "Lỗi")
    }
  }
  GetUserId() {
    let user = this.authService.getAccessUser();
    if (user != null && user != undefined) {
      this.userId = user.id;
      this.binhLuan.userName = user.userName;
    }

  }
  FireBase() {
    this.messagingService.getFcmToken();
    let token = this.messagingService.getFcmToken();
    this.apiService.get<any>("Home/TestFireBase?token="+token)
      .subscribe(result => {
       console.log("Test FireBase: ",result)
      },
      (err: any) => {
       
        console.log("Lỗi: ",err)
      });
  }
  Dang() {
    this.GetUserId();
    this.binhLuan.userId = this.userId;
    this.apiService.post<any>("Home/Comment", this.binhLuan)
      .subscribe(result => {
        console.log("Binh luan: ", result)
        //window.location.reload();
        this.GetDetail(this.id);
        
        if(this.detailNews.userId != this.binhLuan.userId){
          this.fireBaseService.addData(result,"/message/"+result.userReceived)
        }
        
        this.toastr.success("Bình luận thành công", "Thành công")
      },
      (err: any) => {
        window.location.reload();

      });
  }
}
