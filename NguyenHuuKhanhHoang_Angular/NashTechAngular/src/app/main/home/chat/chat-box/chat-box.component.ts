import { Component, OnInit } from '@angular/core';
import { AngularFirestore , AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
interface Post{
  admin:string;
  user:string;
}
@Component({
  selector: 'app-chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.css']
})

export class ChatBoxComponent implements OnInit {
  testCol:AngularFirestoreCollection<Post>;
  test:Observable<Post[]>
  a:Observable<any>
  chat:any;
  constructor(private db: AngularFirestore) { }

  ngOnInit(): void {
    
      this.get().subscribe(a=>{
       
        this.chat =a.data.Messager
        console.log(this.chat)
      });
     
    //console.log(this.db.collection('chat-message').snapshotChanges())
  }
  get(){
    return this.db
    .collection<any>('chat-message')
    .doc('1')
    .snapshotChanges()
    .pipe(
      map(doc=>{
        return { id:doc.payload.id, data:doc.payload.data() as any}
      })
    );
  }
}
