import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { News } from 'src/app/main/home/home.model';

@Component({
  selector: 'app-news-body',
  templateUrl: './news-body.component.html',
  styleUrls: ['./news-body.component.css']
})
export class NewsBodyComponent implements OnInit {
  bodyNews: News[];
  bodyNewsLayout: News[];
  data: News[];
  takeNews: number = 4;
  pageNumber: number = 1;
  pageCount: number = 0;
  totalRecords: number;
  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit(): void {
    this.bodyNews = new Array<News>();
    this.bodyNewsLayout = new Array<News>();
    this.data = new Array<News>();
    this.GetBody();

  }
  GetBody() {
    this.apiService.get<Array<News>>("Home/GetBodyNews").subscribe(
      resultData => {
        this.bodyNews = [...resultData];
        this.bodyNewsLayout = [...resultData];
        this.totalRecords = this.bodyNewsLayout.length;
      }, (err: any) => {
        console.log("LOiooioioioi", err)
      });
  }


}
