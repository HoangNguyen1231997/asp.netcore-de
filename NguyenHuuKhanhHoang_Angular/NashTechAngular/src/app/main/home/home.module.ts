import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { TopHeaderComponent } from './top-header/top-header.component';
import { NewsBodyComponent } from './news-body/news-body.component';
import { NewsRightComponent } from './news-right/news-right.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NewsDetailComponent } from './news-detail/news-detail.component';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { DangBaiVietComponent } from './dang-bai-viet/dang-bai-viet.component';
import { ThongTinUserComponent } from './thong-tin-user/thong-tin-user.component';
import { SuaBaiVietComponent } from './sua-bai-viet/sua-bai-viet.component';
import { TestFirebaseComponent } from './test-firebase/test-firebase.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ChatBoxComponent } from './chat/chat-box/chat-box.component';

@NgModule({
  declarations: [HomeComponent, TopHeaderComponent, NewsBodyComponent, NewsRightComponent, NewsDetailComponent, HeaderComponent, DangBaiVietComponent, ThongTinUserComponent, SuaBaiVietComponent, TestFirebaseComponent, ChatBoxComponent],
  imports: [
    CommonModule, NgxPaginationModule,NgxSpinnerModule,
    HomeRoutingModule, FormsModule
  ], exports: [
    TopHeaderComponent, NewsBodyComponent, NewsRightComponent, NewsDetailComponent, HeaderComponent
  ],
})
export class HomeModule { }
