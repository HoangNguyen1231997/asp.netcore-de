export class Home {
}
export class News {
    constructor(
        public tieuDe: string = null,
        public theLoai: string = null,
        public Type: string = null,
        public userId: number = null,
        public newsDetail: NewsDetail = null,
        public comments: Comments[] = [],
        public user: User = null,
    ) { }
}
export class NewsDetail {
    constructor(
        public noiDung: string = null,
        public noiDungSub: string = null,
        public thoiGianDang: string = null,
        public imageUrl: string = null,
        public imageUrlSub: string = null,
    ) { }
}
export class Comments {
    constructor(
        public noiDung: string = null,
        public newsId: number = null,
        public userId: number = null,
        public userName: string = null,
    ) { }
}
export class User {
    constructor(
        public name: string = null,
        public gioiThieu: string = null,
        public news: News[] = []
    ) { }
}
export class UploadNews {
    constructor(
        public TieuDe: string = null,
        public UserId: number = null,
        public TheLoai: string = null,
        public NoiDung: string = null,
        public NoiDungSub: string = null,
        public ImageUrl: string = null,
        public ImageUrlSub: string = null,
    ) { }
}
export class UpdateNews {
    constructor(
        public id: number = null,
        public newDetailId: number = null,
        public tieuDe: string = null,
        public userId: number = null,
        public theLoai: string = null,
        public noiDung: string = null,
        public noiDungSub: string = null,
        public imageUrl: string = null,
        public imageUrlSub: string = null,
    ) { }
}
export class FireBaseTest {
    constructor(
        public Body: string = null,
        public IsRead: string = null,
        public Link: string = null,
        public Title: string = null,
        public UserId: number = null
    ) { }
}
export class FireBaseTestValue {
    constructor(
        public key:string = null,
        public Body: string = null,
        public IsRead: string = null,
        public isClick: boolean = null,
        public link: string = null,
        public Title: string = null,
        public UserId: number = null
    ) { }
}
export class Data {
    constructor(
        public Body: string = null,
        public Title: string = null
    ) { }
}
