import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { ApiService } from 'src/app/service/api.service';
import { News, NewsDetail, UploadNews } from 'src/app/main/home/home.model';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var require: any
@Component({
  selector: 'app-dang-bai-viet',
  templateUrl: './dang-bai-viet.component.html',
  styleUrls: ['./dang-bai-viet.component.css']
})
export class DangBaiVietComponent implements OnInit {
  userName: string
  fileupLoad: File = null
  fileanhPhuupLoad: File = null
  fileAmazonupLoad: File = null
  news: UploadNews;
  tieuDeValidate:string=null;
  noidungValidate:string=null;
  noidungSubValidate:string=null;
  theLoaiValidate:string=null;
  constructor(private authService: AuthService, private apiService: ApiService, private router: Router
    , private toastr: ToastrService) { }

  ngOnInit(): void {
    this.news = new UploadNews();
    this.GetNameUser();
  }
  fileSelected(event) {
    this.fileupLoad = event.item(0);
  }
  fileSelectedAMZ(event) {
    this.fileAmazonupLoad = event.item(0);
  }
  fileanhPhuSelected(event) {
    console.log("Chon anh: ",event)
    this.fileanhPhuupLoad = event.item(0);
  }
  GetNameUser() {
    let user = this.authService.getAccessUser();
    
    if (user != null && user != undefined) {
      this.userName = user.userName;
      this.news.UserId = user.id;
    }

  }
  UploadAnhChinh() {
    this.apiService.postFile(this.fileupLoad.name, this.fileupLoad).subscribe(
      data => {
        this.news.ImageUrl = data.value;
        this.UploadAnhPhu();
      },(err: any) => {
        console.log("Anh chinh loi",err)
      }
    );
  }
  UploadAnhToAMZ() {
    // this.apiService.postFileAMZ(this.fileupLoad.name, this.fileupLoad).subscribe(
    //   data => {
    //     console.log("UploadAnhToAMZ",data)
        
    //   },(err: any) => {
    //     console.log("Anh chinh loi",err)
    //   }
    // );
  }
  UploadAnhPhu() {
    this.apiService.postFile(this.fileanhPhuupLoad.name, this.fileanhPhuupLoad).subscribe(
      data => {
        this.news.ImageUrlSub = data.value;
        this.UploadNews();
      }
    );
  }
  UploadNews() {
    console.log("test",this.news)
    this.apiService.post<any>("Home/DangBaiViet", this.news)
      .subscribe(result => {

        this.router.navigate(['/home']);
        this.toastr.success("Đăng bài thành công", "Thành công")
      },
      (err: any) => {
        console.log("Loi",err)
        if (err.error.Errors != undefined) {
          err.error.Errors.forEach(element => {
            if (element.Field == "TieuDe") {
              this.tieuDeValidate = element.Message;
            }
            if (element.Field == "NoiDung") {
              this.noidungValidate = element.Message;
            }
            if (element.Field == "NoiDungSub") {
              this.noidungSubValidate = element.Message;
            }
            if (element.Field == "TheLoai") {
              this.theLoaiValidate = element.Message;
            }
          });
          err.error.Message = "Đăng bài thất bại"
        }else{
          window.location.reload();
        }
        this.toastr.error(err.error.Message, "Lỗi")
      });
  }
  Onsubmit() {
    if (this.fileupLoad == undefined || this.fileupLoad == null) {
      this.toastr.error("Bạn cần upload ảnh chính", "Lỗi")
    }
    if (this.fileanhPhuupLoad == undefined || this.fileanhPhuupLoad == null) {
      this.toastr.error("Bạn cần upload ảnh phụ", "Lỗi")
    }
    if(this.fileupLoad != undefined && this.fileupLoad != null && this.fileanhPhuupLoad != undefined || this.fileanhPhuupLoad != null ){
      this.tieuDeValidate=null;
      this.theLoaiValidate=null;
      this.noidungSubValidate=null;
      this.noidungValidate=null;
      this.UploadAnhChinh();
    }
  }
 
}
