import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DangBaiVietComponent } from './dang-bai-viet.component';

describe('DangBaiVietComponent', () => {
  let component: DangBaiVietComponent;
  let fixture: ComponentFixture<DangBaiVietComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DangBaiVietComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DangBaiVietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
