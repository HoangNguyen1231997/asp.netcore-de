import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { News } from 'src/app/main/home/home.model';

@Component({
  selector: 'app-top-header',
  templateUrl: './top-header.component.html',
  styleUrls: ['./top-header.component.css']
})
export class TopHeaderComponent implements OnInit {

  constructor(private router: Router, private apiService: ApiService) { }
  headerNews: News[];
  ngOnInit(): void {
    this.headerNews = new Array<News>();
    this.GetHeader();
  }
  GetHeader() {
    this.apiService.get<Array<News>>("Home/GetHeaderNews").subscribe(
      resultData => {
        this.headerNews = [...resultData];
      }, (err: any) => {
        console.log("LOiooioioioi", err)
      });
  }
}
