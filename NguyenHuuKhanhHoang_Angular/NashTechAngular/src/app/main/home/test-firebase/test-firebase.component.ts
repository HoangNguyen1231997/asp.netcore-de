import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireList, DataSnapshot } from '@angular/fire/database/interfaces';
import { Observable } from '@firebase/util';
import { map } from 'rxjs/operators';
import { FireBaseTest, Data } from 'src/app/main/home/home.model';
import { FireBaseDatabaseService } from 'src/app/service/fire-base-database.service';
import { ApiService } from 'src/app/service/api.service';
import { ToastrService } from 'ngx-toastr';
import { RequestOptions, ResponseContentType, Http, Headers } from '@angular/http';
import { saveAs } from 'file-saver';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;
@Component({
  selector: 'app-test-firebase',
  templateUrl: './test-firebase.component.html',
  styleUrls: ['./test-firebase.component.css']
})
export class TestFirebaseComponent implements OnInit {
  notification: AngularFireList<any[]>;
  typeofInput = [
    '.doc',
    '.docx',
    '.jpg',
    '.jpeg',
    '.jfif',
    '.pdf',
    '.png',
    '.xls',
    '.xlsx'
  ]
  @ViewChild('userPhoto') userPhoto: ElementRef;
  notification2: any;
  a: Observable<any>;
  addNotification: FireBaseTest;
  klkl: Array<Data>;
  fileAmazonupLoad: File = null
  constructor(private spinner: NgxSpinnerService,private https: HttpClient, private db: AngularFireDatabase, private http: Http, private toastr: ToastrService, private apiService: ApiService, private fireBaseService: FireBaseDatabaseService) {

  }
  @ViewChild('form') form;
  ngOnInit(): void {
    
    this.klkl = new Array<Data>();
    this.addNotification = new FireBaseTest();
    //this.notification = this.db.list('/notification/1');
    //this.a= this.db.list('/notification')
    this.notification = this.db.list('/notification');
    console.log("hello 123: ", this.notification)

    this.db.list('/notification').snapshotChanges().forEach(element => {
      console.log("OKOK", element)
      element.forEach(a => {
        console.log("key: ", a.key)
        console.log("Please: ", a.payload.toJSON())
      })
    });
    this.db.list('/notification').snapshotChanges().subscribe(x => {


      console.log("asdasdsdads", x.values())

    });

    this.fireBaseService.getData('/message/user_2').forEach(element => {
      console.log("fireBaseService", element)
      element.forEach(a => {
        console.log("hello: ", a)
        console.log("fireBaseService key: ", a.key)
        console.log("fireBaseService Please: ", a.payload.toJSON())
        //this.klkl = a.payload.toJSON()
        console.log("a.payload.val(): ", a.payload.val())
        this.notification2 = a.payload.val();
        this.klkl.push(this.notification2)
        console.log("this.notification2.Body: ", this.notification2)

      })
    });
    console.log("this.klkl: ", this.klkl)
  }
  hello(data: DataSnapshot) {
    //his.klk
  }
  createFireBase() {
    this.addNotification.Body = "Test Add Fire Base"
    this.addNotification.IsRead = "False"
    this.addNotification.Title = "Thong bao"
    this.addNotification.Link = "https://www.google.com/"
    this.addNotification.UserId = 1
    this.db.list('/notification').push(this.addNotification);
  }
  createFireBase2() {
    this.db.list('/message/user_1').push({
      Body: "Test nao 6",
      Title: "Thong bao"
    });
  }

  updateFireBase() {

    this.db.object('/notification/2').update({
      Body: "UpdateFireBase123131323122131"
    });
  }
  deleteFireBase() {

    this.db.object('/notification/-M6y3ThMndNvwlTDRBQq').remove();
  }
  fileSelectedAMZ(event) {
    
    console.log("fileSelectedAMZ: ", event)
    let typeofFile = event.item(0).name.slice(event.item(0).name.indexOf("."))
    if (this.typeofInput.find(x => x == typeofFile) != undefined) {
      console.log("Đúng kiểu ảnh rồi đó", this.typeofInput.find(x => x == typeofFile))
      this.fileAmazonupLoad = event.item(0);
    } else {
      this.toastr.error("Không hỗ trợ loại file này vui lòng chọn lại", "Lỗi")
      //event= new Array<FileList>();
      this.userPhoto.nativeElement.value = null;
      console.log("Sau khi reset: ", event)
    }

  }
  Download() {
    this.spinner.show();
    this.apiService.post<any>('AWS/GetTaiLieuUrl?bucketName=khanhhoangtestaws&keyName=hoangfolder/grapefruit-slice-332-332.jpg').subscribe((resp) => {
      //this.downloadFile("https://cors-anywhere.herokuapp.com/" + resp.value, "grapefruit-slice-332-332.jpg");
      this.exportPdf("https://cors-anywhere.herokuapp.com/" + resp.value);
      
    })
  }
  DownloadPDF(){
    this.spinner.show();
    this.apiService.post<any>('AWS/GetTaiLieuUrl?bucketName=khanhhoangtestaws&keyName=hoangfolder/3d3619f9-2f4c-48ba-93a3-c86a8bf90fb0_HNVN_TR_OnlineTrainingTool_Guideline_Handout.pdf').subscribe((resp) => {
      //this.downloadFile("https://cors-anywhere.herokuapp.com/" + resp.value, "grapefruit-slice-332-332.jpg");
      this.exportPdf("https://cors-anywhere.herokuapp.com/" + resp.value);
      
    })
  }
  exportPdf(url) {
    this.https.get(url,
      { responseType: 'blob' }).subscribe(data => 
        {saveAs(data,"file");
      this.spinner.hide();
        });
  }
  export(url) {
    return this.https.get(url,
      { responseType: 'blob' });
  }
  downloadFile(url: string, fileName: string) {
    
    const options = new RequestOptions({ responseType: ResponseContentType.Blob });
    // Process the file downloaded
    this.http.get(url, options).subscribe(res => {
      //const fileName = getFileNameFromResponseContentDisposition(res);
      this.saveFile(res.blob(), fileName);
    });
  }
  Hienthi(){
    this.apiService.post<any>('AWS/GetTaiLieuUrl?bucketName=khanhhoangtestaws&keyName=hoangfolder/grapefruit-slice-332-332.jpg').subscribe((resp) => {
      var html="<div class=\"showFile\">";
            html+="<div class=\"middle\">";
            html+="<div class=\"inner\">";     
            html+="<span class=\"close\" onclick=\"$('.k-overlay').hide();$('.showFile').detach();\"><i class=\"ft-x\"></i></span>";
            html+="<img src='"+resp.value+"' alt='' style=\"max-width: 770px;max-height: 570px;\"/>";
            html+="</div>";
            html+="</div>";   
            html+="</div>"
            $(".k-overlay").show();
            $(html).appendTo('.wrapper');
      
    })
   
  }
  HienthiPDF(){
    this.apiService.post<any>('AWS/GetTaiLieuUrl?bucketName=khanhhoangtestaws&keyName=hoangfolder/3d3619f9-2f4c-48ba-93a3-c86a8bf90fb0_HNVN_TR_OnlineTrainingTool_Guideline_Handout.pdf').subscribe((resp) => {
      console.log("PDF: ",resp.value)
      var html="<div class=\"showFile\">";
        html+="<div class=\"middle\">";
        html+="<div class=\"inner\">";     
       
        html+="<iframe src='"+ resp.value+"' style=\"width:760px; height:570px;\" frameborder=\"0\"></iframe>";
        html+="</div>";
        html+="</div>";   
        html+="</div>"
        $(".a").show();
        $(".wrapper-pdf").append(html);
      
    })
  }
  saveFile = (blobContent: Blob, fileName: string) => {
    const blob = new Blob([blobContent], { type: 'application/octet-stream' });
    saveAs(blob, fileName);
    console.log("Tai xong")
  }
  UploadAnhToAMZ() {
    this.spinner.show();
    this.apiService.postFileAMZ(this.fileAmazonupLoad.name, this.fileAmazonupLoad,"khanhhoangtestaws","hoangfolder",1).subscribe(
      data => {
        console.log("UploadAnhToAMZ", data)
        this.userPhoto.nativeElement.value = null;
        this.toastr.success("Upload file thành công","Thông báo")
        this.spinner.hide();
      }, (err: any) => {
        console.log("err", err)
        this.toastr.error(err,"Lỗi")
        this.spinner.hide();
      }
    );
  }
  reset() {
    console.log("hehehehehehehe: ", this.form.nativeElement.reset())
    this.form.nativeElement.innerHTML = "Sai"
  }
}