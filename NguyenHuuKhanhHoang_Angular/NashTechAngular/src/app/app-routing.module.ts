import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/main/home/home/home.component';
import { LoginComponent } from 'src/app/main/login/login/login.component';
import { NewsDetailComponent } from 'src/app/main/home/news-detail/news-detail.component';
import { DangBaiVietComponent } from 'src/app/main/home/dang-bai-viet/dang-bai-viet.component';
import { RegisterComponent } from 'src/app/main/login/register/register.component';
import { ThongTinUserComponent } from 'src/app/main/home/thong-tin-user/thong-tin-user.component';
import { SuaBaiVietComponent } from 'src/app/main/home/sua-bai-viet/sua-bai-viet.component';
import { AuthenGuardService } from 'src/app/service/authen-guard.service';
import { TestFirebaseComponent } from 'src/app/main/home/test-firebase/test-firebase.component';
import { ChatBoxComponent } from './main/home/chat/chat-box/chat-box.component';

const routes: Routes= [
  {path: '',redirectTo:'/home',pathMatch: 'full'},
  {path: 'home',component: HomeComponent},
  {path: 'chat',component: ChatBoxComponent},
  {path: 'test-firebase',component: TestFirebaseComponent},
  {path: 'login',component: LoginComponent},
  {path: 'chi-tiet/:id',component: NewsDetailComponent,
   canActivate :[AuthenGuardService], 
   data:{isCheck:false}},
  {path: 'dang-bai-moi',component: DangBaiVietComponent,
  canActivate :[AuthenGuardService], 
  data:{isCheck:true}},
  {path: 'dang-ky',component: RegisterComponent},
  {path: 'thong-tin',component: ThongTinUserComponent},
  {path: 'sua-bai-viet/:id',component: SuaBaiVietComponent,
  canActivate :[AuthenGuardService], 
  data:{isCheck:false}},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
