import { Injectable } from '@angular/core';

import { AccessUser, AccessToken } from 'src/app/model/auth.model';
import { LocalStorageHelper } from 'src/app/model/local-storage-helper';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/service/api.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private AccessUserKey = 'AccessUserKey';
  constructor(private router: Router,private toastr: ToastrService) { }
  setAuth(accessUser: AccessUser) {
    LocalStorageHelper.setItem(this.AccessUserKey, accessUser);
  }
  getToken(): AccessToken {
    const accessUser = this.getAccessUser();
    if (accessUser != null) {
      return accessUser.accessToken;
    }
    return null;
    
  }
  getAccessUser(): AccessUser {
    return LocalStorageHelper.getItem<AccessUser>(this.AccessUserKey)
  }
  destroyAccessUser() {
    LocalStorageHelper.removeItem(this.AccessUserKey);
  }
  destroyFCMToken() {
    LocalStorageHelper.removeItem("FCMToken");
  }
  
  logout() {
    this.destroyAccessUser();
    this.destroyFCMToken();

    // this.router.navigate(['login']);
    // this.toastr.success("Đăng xuất thành công", "Thành công")
  }
}
