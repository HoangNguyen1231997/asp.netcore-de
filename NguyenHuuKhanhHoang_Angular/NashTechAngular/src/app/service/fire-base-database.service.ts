import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class FireBaseDatabaseService {
  notification:AngularFireList<any[]>;
  constructor(private db: AngularFireDatabase) { }
  getData(name:any){
    return this.db.list(name).snapshotChanges();
  }
  getData2(name:any){
    return this.db.list(name);
  }
  addData(data:any,name:string){
    this.db.list(name).push(data);
  }
  updateData(data:any,name:string){
    this.db.object(name).update(data);
  }
  updateDataClick(data:any,name:string){
    this.db.object(name).update({
      isClick:true
    });
  }
}
