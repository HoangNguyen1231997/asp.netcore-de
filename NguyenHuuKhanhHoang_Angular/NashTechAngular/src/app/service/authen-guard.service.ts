import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { ApiService } from 'src/app/service/api.service';
import { ToastrService } from 'ngx-toastr';
import { map, catchError,take } from 'rxjs/operators';
import { of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthenGuardService implements CanActivate {

  constructor(private apiService: ApiService, private router: Router,
    private toastr: ToastrService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean>|Observable<boolean> | boolean {
    
    let isCheck = route.data.isCheck;
    if (isCheck == true) {

      return new Promise(res => {
        this.apiService.get<any>("Home/CheckPermission").subscribe(
            (data) => {
                if (data === null) {
                    res(true);
                } 
            },
            (error) => {
              this.router.navigate(['/home']);
              this.toastr.error("Bạn không có quyền thực hiện chức năng này", "Lỗi")
                res(false);
            }
        );
    });
    } else {
     
      return new Promise(res => {
        this.apiService.get<any>("Home/CheckToken").subscribe(
            (data) => {
                if (data === null) {
                    res(true);
                } 
            },
            (error) => {
              this.router.navigate(['/login']);
              this.toastr.error("Bạn cần đăng nhập trước khi đọc nội dung", "Lỗi")
                res(false);
            }
        );
    });
    }
  }
}
