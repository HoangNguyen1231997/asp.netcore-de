import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { ObjectUnsubscribedError } from 'rxjs/internal/util/ObjectUnsubscribedError';
import { AuthService } from 'src/app/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private authService: AuthService) { }
  post<T>(path: string, body: any = null): Observable<T> {
    var token = this.GetToken();

    if (token) {
      var headers = new HttpHeaders().set('Authorization', `Bearer ${token.token}`);
      return this.http.post<T>(`https://localhost:5001/${path}`, body, { headers: headers })
    } else {
      var headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
      return this.http.post<T>(`https://localhost:5001/${path}`, body, { headers: headers })
    }
  }

  get<T>(path: string, params: HttpParams = new HttpParams()): Observable<T> {
    var token = this.GetToken();
    if (token) {
      var headers = new HttpHeaders().set('Authorization', `Bearer ${token.token}`);
      return this.http.get<T>(`https://localhost:5001/${path}`, { headers: headers });
    } else {

      return this.http.get<T>(`https://localhost:5001/${path}`);
    }

  }
  GetToken() {
    const accessToken = this.authService.getToken();
    return accessToken;
  }
  setTokenFCMUser(id:number , token:string){
    this.get<any>("Login/SetTokenFCM?userId="+id+"&tokenFCM="+token)
    .subscribe(result => {
    },
    (err: any) => {
    });
  }
  postFile(name: string, file: File) {
    const url = 'https://localhost:5001/Home/UploadImage';
    const formData: FormData = new FormData();
    formData.append('Image', file, name);
    return this.http.post<any>(url, formData);
  }
  postFileAMZ(name: string, file: File,bucketName:string, folder:string,userId:number) {
    console.log("userId",userId)
    const url = 'https://localhost:5001/AWS/TestAWSIformfile?bucketName='+bucketName+'&folder='+folder+'&userId='+userId;
    const formData: FormData = new FormData();
    formData.append('Image', file, name);
    return this.http.post<any>(url, formData);
  }

}
