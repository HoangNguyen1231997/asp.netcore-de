import { Injectable, ChangeDetectorRef } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject, Subscription } from 'rxjs'
import * as firebase from 'firebase/app';
import '@firebase/messaging';
import { LocalStorageHelper } from 'src/app/model/local-storage-helper';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  

  
  currentMessage = new BehaviorSubject(null);
  private subscription: Subscription;
  constructor(private angularFireMessaging: AngularFireMessaging,private router: Router,private toastr: ToastrService) {
    // this.angularFireMessaging.messaging.subscribe(
    //   (_messaging) => {
    //     _messaging.onMessage = _messaging.onMessage.bind(_messaging);
    //     _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    //   }
    // );
    
    this.angularFireMessaging.messages.subscribe(data=>console.log("Troi12313213: ",data))
    this.angularFireMessaging.messaging.subscribe(
      (_messaging: any) => {
        // _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging._next = (payload: any) => {
          console.log(this.angularFireMessaging,this.angularFireMessaging.messaging);
          
          console.log("payloadpayload: ",payload)
            this.currentMessage.next(payload)
        
          //this.angularFireMessaging.getToken.subscribe(x=>console.log("xxxxxx",x))
          // this.angularFireMessaging.deleteToken("d4Fzozz5pPZu0mEPYiF2n9:APA91bHAbeVt-vN8BN_37YoW7nMkHXcyf-WPh9cKS9ix8DA1GFS4lGOhvJDyihNkie6gZx7euaC3OLMj0egMTkYcMKKxiRXWACADbRZpZp1Tqh6z3y-KRlbI4fwxroNUcMB4HvVxBvrc")
          // .subscribe(result=>console.log("Hehe",result));
        };
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    );
  }
  listenToNotifications() {
    return this.angularFireMessaging.messages;
  }

  setFcmToken(token) {
    LocalStorageHelper.setItem("FCMToken", token)
  }
  getFcmToken(): string {
    return LocalStorageHelper.getItem<string>("FCMToken")
  }
  deleteToken() {
    this.angularFireMessaging.getToken.subscribe(token=>{
      this.angularFireMessaging.deleteToken(token)
      .subscribe(result=>{
        //this.requestPermission()
        this.angularFireMessaging.requestToken.subscribe(
          (token) => {
           
          },
          (err) => {
            console.error('Unable to get permission to notify.', err);
          }
        );
        this.router.navigate(['login']);
        this.toastr.success("Đăng xuất thành công", "Thành công")
      });
     
    })
    
  }
  requestPermission() {

   this.subscription= this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log("Troi: ", token);
        this.setFcmToken(token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }
  onMessageReceived() {
   this.subscription=this.angularFireMessaging.messages.subscribe(x=>this.currentMessage.next(x));
  }
}
