// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAc_fxnRl7HCZ7gCAwZW3VB36ihs-s0SD8",
    authDomain: "websosanh-1538826415686.firebaseapp.com",
    databaseURL: "https://websosanh-1538826415686.firebaseio.com",
    projectId: "websosanh-1538826415686",
    storageBucket: "websosanh-1538826415686.appspot.com",
    messagingSenderId: "389007036229",
    appId: "1:389007036229:web:218e099c6b7c0d238db82c",
    measurementId: "G-YZ6GRKFECK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
